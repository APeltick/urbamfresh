<?php
/**
 * Booster for WooCommerce - Module - Currency per Product
 *
 * @version 3.2.1
 * @since   2.5.2
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'WC_Currency_Per_Product' ) ) :

class WC_Currency_Per_Product {

	/**
	 * Constructor.
	 *
	 * @version 2.8.0
	 * @since   2.5.2
	 */
	function __construct() {
		// Currency code and symbol
		add_filter( 'woocommerce_currency_symbol',                array( $this, 'change_currency_symbol' ),     PHP_INT_MAX, 2 );
		add_filter( 'woocommerce_currency',                       array( $this, 'change_currency_code' ),       PHP_INT_MAX );

		// Add to cart
		add_filter( 'woocommerce_add_cart_item_data',             array( $this, 'add_cart_item_data' ),         PHP_INT_MAX, 3 );
		add_filter( 'woocommerce_add_cart_item',                  array( $this, 'add_cart_item' ),              PHP_INT_MAX, 2 );
		add_filter( 'woocommerce_get_cart_item_from_session',     array( $this, 'get_cart_item_from_session' ), PHP_INT_MAX, 3 );
	}

	/**
	 * get_product_currency.
	 *
	 * @version 2.9.0
	 * @since   2.9.0
	 */
	function get_product_currency( $product_id ) {
		$post = get_post( $product_id );

		if( !empty( $post->post_author ) ) {
			$settings = get_user_meta( $post->post_author, 'dokan_profile_settings', true );
			if( isset( $settings['currency'] ) ) {
				return $settings['currency'];
			}
		}
		return false;
	}

	/**
	 * get_currency_exchange_rate.
	 *
	 * @version 2.5.2
	 * @since   2.5.2
	 */
	function get_currency_exchange_rate( $currency_code ) {
		$currency_exchange_rate = 1;
		$total_number = apply_filters( 'booster_get_option', 1, get_option( 'wcj_currency_per_product_total_number', 1 ) );
		for ( $i = 1; $i <= $total_number; $i++ ) {
			if ( $currency_code === get_option( 'wcj_currency_per_product_currency_' . $i ) ) {
				$currency_exchange_rate = 1 / get_option( 'wcj_currency_per_product_exchange_rate_' . $i );
				break;
			}
		}
		return $currency_exchange_rate;
	}


	/**
	 * get_cart_item_from_session.
	 *
	 * @version 2.5.2
	 * @since   2.5.2
	 */
	function get_cart_item_from_session( $item, $values, $key ) {
		if ( array_key_exists( 'product_currency', $values ) ) {
			$item['data']->product_currency = $values['product_currency'];
		}
		return $item;
	}

	/**
	 * add_cart_item_data.
	 *
	 * @version 2.9.0
	 * @since   2.5.2
	 */
	function add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
		$currency_per_product_currency = $this->get_product_currency( $product_id );
		if ( '' != $currency_per_product_currency ) {
			$cart_item_data['product_currency'] = $currency_per_product_currency;
		}
		return $cart_item_data;
	}

	/**
	 * add_cart_item.
	 *
	 * @version 2.5.2
	 * @since   2.5.2
	 */
	function add_cart_item( $cart_item_data, $cart_item_key ) {
		if ( isset( $cart_item_data['product_currency'] ) ) {
			$cart_item_data['data']->product_currency = $cart_item_data['product_currency'];
		}
		return $cart_item_data;
	}

	/**
	 * get_currency.
	 *
	 * @version 2.9.0
	 * @since   2.7.0
	 */
	function get_currency() {
		// Get ID
		$the_ID = false;
		global $product, $uf_currency, $uf_seller_id;
		if( !empty( $uf_currency ) ) {
			return $uf_currency;
		}

		if ( is_object( $product ) ) {
			$the_ID = $this->get_product_id_or_variation_parent_id( $product );
		}
		if ( ! $the_ID && isset( $_REQUEST['product_id'] ) ) {
			$the_ID = $_REQUEST['product_id'];
		}

		if ( ! $the_ID ) {
			$the_ID = get_the_ID();
		}

		// Get currency
		if ( $the_ID && 'product' === get_post_type( $the_ID ) ) {
			$post = get_post( $the_ID );
			$author_id = $post->post_author;
		}

		if( !empty( $uf_seller_id ) ) {
			$author_id = $uf_seller_id;
		}

		if( !empty( $author_id ) ) {
			$settings = get_user_meta( $author_id, 'dokan_profile_settings', true );

			if( isset( $settings['currency'] ) ) {
				return $settings['currency'];
			}
		}

		return false;
	}

	function get_product_id_or_variation_parent_id( $_product ) {
		return ( $_product->is_type( 'variation' ) ) ? $_product->get_parent_id() : $_product->get_id();
	}

	/**
	 * get_cart_checkout_currency.
	 *
	 * @version 2.8.0
	 * @since   2.7.0
	 */

	function get_cart_checkout_currency() {
		$cart_items = array_reverse( WC()->cart->get_cart() );
		foreach ( $cart_items as $cart_item ) {
			if( isset( $cart_item['product_currency'] ) ) {
				return $cart_item['product_currency'];
			} else if( isset( $cart_item['product_id'] ) ) {
				$post = get_post( $cart_item['product_id'] );
				if( isset( $post->post_author ) ) {
					$settings = get_user_meta( $post->post_author, 'dokan_profile_settings', true );
					if( isset( $settings['currency'] ) ) {
						return $settings['currency'];
					}
				}
			}
		}
		return false;
	}

	/**
	 * is_cart_or_checkout_or_ajax.
	 *
	 * @version 2.7.0
	 * @since   2.7.0
	 * @todo    fix AJAX issue (for minicart)
	 */
	function is_cart_or_checkout_or_ajax() {
		return ( ( function_exists( 'is_cart' ) && is_cart() ) || ( function_exists( 'is_checkout' ) && is_checkout() ) /* || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) */ );
	}

	/**
	 * change_currency_code.
	 *
	 * @version 2.7.0
	 * @since   2.5.2
	 */
	function change_currency_code( $currency ) {
		if ( false != ( $_currency = $this->get_currency() ) ) {
			return $_currency;
		} elseif ( $this->is_cart_or_checkout_or_ajax() ) {
			return ( false != ( $_currency = $this->get_cart_checkout_currency() ) ) ? $_currency : $currency;
		}
		return  $currency;
	}

	/**
	 * change_currency_symbol.
	 *
	 * @version 2.7.0
	 * @since   2.5.2
	 */
	function change_currency_symbol( $currency_symbol ) {
		if ( false != ( $_currency = $this->get_currency() ) ) {
			return $this->get_currency_symbol( $_currency );
		} elseif ( $this->is_cart_or_checkout_or_ajax() ) {
			return ( false != ( $_currency = $this->get_cart_checkout_currency() ) ) ? $this->get_currency_symbol( $_currency ) : $currency_symbol;
		}
		return $currency_symbol;
	}

	function get_currency_symbol( $key ) {
		$currencies = get_option( 'woocs', array() );
		if( isset( $currencies[ $key ]['symbol'] ) ) {
			return $currencies[ $key ]['symbol'];
		}
		return '';
	}

}

endif;

return new WC_Currency_Per_Product();
