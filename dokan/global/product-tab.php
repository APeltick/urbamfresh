<?php
/**

 * Dokan Seller Single product tab Template

 *

 * @since 2.4

 *

 * @package dokan

 */
global $product;
?>

<div class="product_details_additionalinfo">
        <div>
            <h5><?php _e( 'Sellers Info', 'dokan-lite' ); ?></h5>

             <?php if ( !empty( $store_info['address'] ) ) { ?>
                <span class="details">
                    <?php
                    echo esc_html( $store_info['store_name'] ); ?><br/>
                    <?php echo dokan_get_seller_address( $author->ID ); ?><br/>
                </span>
                <a class='open_contact_form_popup' href="#contact_form_popup">Contact Us</a>
            <?php } ?>
            
            
            </div>
            <div>
                <h5>Store Hours</h5>
                <p>
                    <?php if( isset( $store_info['shop_hours_type'] ) && $store_info['shop_hours_type'] == 'appointment_only' ) {
                        echo '<i class="fa fa-clock-o"></i> ';
                        _e('Appointment only');
                    } else {
                        if ( !empty($store_info['store_from']) && !empty($store_info['store_to']) ) {
                            echo '<i class="fa fa-clock-o"></i> ';
                            echo esc_html( $store_info['store_from'] . "-" . $store_info['store_to'] );
                        } else {
                            echo '<i class="fa fa-clock-o"></i> ';
                            echo 'Open 24 hours';
                        }
                        echo '<br />';
                        echo uf_get_shop_days( isset( $store_info['store_days'] ) ? $store_info['store_days'] : '' );
                    } ?>
                </p>
            </div>

</div>
<div id="contact_form_popup" class="uf_form_popup">
    <label>
        Your Name (required)<br />
        <input type="text" name="name" value="" />
    </label>

    <label>
        Your Email (required)<br />
        <input type="email" name="email" value="" />
    </label>
    <label>
        Your Message<br />
        <textarea rows="5" cols="50" name="message"></textarea>
    </label>

    <button class="primary_button" id="contact_form_send" data-user_id="<?php echo $author->ID ?>" data-product_id="<?php echo $product->get_id() ?>">Send</button>
</div>
