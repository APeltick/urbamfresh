<?php
/**
 * Dokan Seller registration form
 *
 * @since 2.4
 *
 * @package dokan
 */
?>

<input type="hidden" name="role" value="seller" />
<?php do_action( 'dokan_reg_form_field' ); ?>