<?php global $post;
wp_enqueue_script('uf-jquery-mask', get_stylesheet_directory_uri() . '/inc/mask/jquery.numbox-1.3.0.min.js');
wp_enqueue_script('product-listing-ajax', get_stylesheet_directory_uri() . '/assets/js/product-listing-ajax.js');
wp_localize_script('product-listing-ajax', 'product_listing', array(
    'ajax_url' => get_site_url() . "/wp-admin/admin-ajax.php"
));


if( isset( $_GET['add_product'] ) ) { ?>
    <script type="text/javascript">
        var uf_add_product = 1;
		window.history.pushState('page2', 'Title', '/myshop/products/');
    </script>
<?php } ?>



<div class="dokan-dashboard-wrap">



    <?php



    /**

     *  dokan_dashboard_content_before hook

     *

     *  @hooked get_dashboard_side_navigation

     *

     *  @since 2.4

     */

    do_action( 'dokan_dashboard_content_before' );

    ?>



    <div class="dokan-dashboard-content dokan-product-listing">



        <?php



        /**

         *  dokan_dashboard_content_before hook

         *

         *  @hooked get_dashboard_side_navigation

         *

         *  @since 2.4

         */

        do_action( 'dokan_dashboard_content_inside_before' );

        do_action( 'dokan_before_listing_product' );

        ?>

<?php
if ( is_user_logged_in() ) {
	$current_user = wp_get_current_user();
	$store_info = dokan_get_store_info( $current_user->ID );
	$store_name = $store_info["store_name"];
    //$stick_location   = isset( $store_info['stick_location'] ) ? esc_attr( $store_info['stick_location'] ) : 'no';
}
?>
<h2 style="font-size: 2em;text-align: center;padding: 0.5em 0;"><?php echo $store_name; ?></h2>
        <article class="dokan-product-listing-area">



            <div class="product-listing-top dokan-clearfix line">

                <?php dokan_product_listing_status_filter(); ?>



                <?php if ( dokan_is_seller_enabled( get_current_user_id() ) ): ?>

                    <span class="dokan-add-product-link">

                            <a href="<?php echo dokan_get_navigation_url( 'new-product' ); ?>" class="dokan-btn dokan-btn-theme dokan-right dokan-btn-lg <?php echo ( 'on' == dokan_get_option( 'disable_product_popup', 'dokan_selling', 'off' ) ) ? '' : 'dokan-add-new-product'; ?>">

                                <i class="fa fa-briefcase">&nbsp;</i>

                                <?php _e( 'Add new product', 'dokan-lite' ); ?>

                            </a>

                        </span>

                <?php endif; ?>

            </div>



            <!--

                <?php dokan_product_dashboard_errors(); ?>



                <div class="dokan-w12">

                    <?php dokan_product_listing_filter(); ?>

                </div>

-->

            <div class="dokan-dahsboard-product-listing-wrapper">

                <table class="dokan-table dokan-table-striped product-listing-table">

                    <thead>

                    <tr>

                        <th><?php _e( 'Image', 'dokan-lite' ); ?></th>

                        <th><?php _e( 'Name', 'dokan-lite' ); ?></th>

                        <th><?php _e( 'Status', 'dokan-lite' ); ?></th>

                        <!--<th><?php _e( 'SKU', 'dokan-lite' ); ?></th>-->

                        <!--<th><?php _e( 'Stock', 'dokan-lite' ); ?></th>-->

                        <th><?php _e( 'Price', 'dokan-lite' ); ?></th>

                        <!--<th><?php _e( 'Type', 'dokan-lite' ); ?></th>-->

                        <!--<th><?php _e( 'Views', 'dokan-lite' ); ?></th>-->

                        <!--<th><?php _e( 'Date', 'dokan-lite' ); ?></th>-->

                    </tr>

                    </thead>

                    <tbody>

                    <?php
                    $pagenum      = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

                    $post_statuses = array('publish', 'draft', 'pending');
                    $args = array(
                        'post_type'      => 'product',
                        'post_status'    => $post_statuses,
                        'posts_per_page' => 10,
                        'author'         => get_current_user_id(),
                        'orderby'        => 'post_date',
                        'order'          => 'DESC',
                        'paged'          => $pagenum,
                        'tax_query'      => array(
                            array(
                                'taxonomy' => 'product_type',
                                'field'    => 'slug',
                                'terms'    => apply_filters( 'dokan_product_listing_exclude_type', array() ),
                                'operator' => 'NOT IN',
                            ),
                        ),
                    );

                    if ( isset( $_GET['post_status']) && in_array( $_GET['post_status'], $post_statuses ) ) {
                        $args['post_status'] = $_GET['post_status'];
                    }

                    if( isset( $_GET['date'] ) && $_GET['date'] != 0 ) {
                        $args['m'] = $_GET['date'];
                    }

                    if( isset( $_GET['product_cat'] ) && $_GET['product_cat'] != -1 ) {
                        $args['tax_query'][] = array(
                            'taxonomy' => 'product_cat',
                            'field' => 'id',
                            'terms' => (int)  $_GET['product_cat'],
                            'include_children' => false,
                        );
                    }

                    if ( isset( $_GET['product_search_name']) && !empty( $_GET['product_search_name'] ) ) {
                        $args['s'] = $_GET['product_search_name'];
                    }

                    $original_post = $post;
                    $product_query = new WP_Query( apply_filters( 'dokan_product_listing_query', $args ) );

                    if ( $product_query->have_posts() ) {
                    while ($product_query->have_posts()) {
                    $product_query->the_post();



                    $tr_class = ($post->post_status == 'draft' ) ? ' class="danger"' : '';

                    $product = wc_get_product( $post->ID );

                    ?>

                    <tr<?php echo $tr_class; ?>>

                        <td data-title="<?php _e( 'Image', 'dokan-lite' ); ?>">

                            <a href="<?php echo dokan_edit_product_url( $post->ID ); ?>"><?php echo $product->get_image(); ?></a>

                        </td>

                        <td data-title="<?php _e( 'Name', 'dokan-lite' ); ?>">

                            <p><a href="<?php echo dokan_edit_product_url( $post->ID ); ?>"><?php echo $product->get_title(); ?></a></p>



                            <div class="row-actions">

                                <span class="edit"><a href="<?php echo dokan_edit_product_url( $post->ID ); ?>"><?php _e( 'Edit', 'dokan-lite' ); ?></a> | </span>

                                <span class="delete"><a onclick="return confirm('Are you sure?');" href="<?php echo wp_nonce_url( add_query_arg( array( 'action' => 'dokan-delete-product', 'product_id' => $post->ID ), dokan_get_navigation_url('products') ), 'dokan-delete-product' ); ?>"><?php _e( 'Delete', 'dokan-lite' ); ?></a> <!--| --></span>

                                <!--<span class="view"><a href="<?php echo get_permalink( $product->get_id() ); ?>" rel="permalink"><?php _e( 'View', 'dokan-lite' ); ?></a></span>-->

                            </div>

                        </td>

                        <td class="post-status" style="vertical-align:middle;" data-uf-post-id = "<?php echo $post->ID; ?>" data-title="<?php _e( 'Status', 'dokan-lite' ); ?>">

                            <label class="dokan-label <?php echo dokan_get_post_status_label_class( $post->post_status ); ?>"><?php echo dokan_get_post_status( $post->post_status ); ?></label>

                        </td>

                        <!--<td data-title="<?php _e( 'SKU', 'dokan-lite' ); ?>">

                                        <?php

                        if ( $product->get_sku() ) {

                            echo $product->get_sku();

                        } else {

                            echo '<span class="na">&ndash;</span>';

                        }

                        ?>

                                    </td>

                                    <td data-title="<?php _e( 'Stock', 'dokan-lite' ); ?>">

                                        <?php

                        if ( $product->is_in_stock() ) {

                            echo '<mark class="instock">' . __( 'In stock', 'dokan-lite' ) . '</mark>';

                        } else {

                            echo '<mark class="outofstock">' . __( 'Out of stock', 'dokan-lite' ) . '</mark>';

                        }



                        if ( $product->managing_stock() ) :

                            echo ' &times; ' . $product->get_total_stock();

                        endif;

                        ?>

                                    </td>

									-->

                        <td style="vertical-align:middle;" data-title="<?php _e( 'Price', 'dokan-lite' ); ?>">

                            <?php

                            //if ( $product->get_price_html() ) {

                                //echo wc_price( wc_get_price_to_display( $product ) );

                                //echo $product->get_price_html();

                            ?>

                             <div class="dokan-input-group uf-price-input">

                                 <span class="dokan-input-group-addon" style="padding: 6px 4px;"><label style="font-weight: 500;" for="uf_ajax_price_<?php echo $product->get_slug(); ?>"><?php echo get_woocommerce_currency_symbol(); ?></label></span>

                                 <input style="float:left" id="uf_ajax_price_<?php echo $product->get_slug(); ?>" data-uf-product-id = "<?php echo $product->get_id(); ?>" type="number" class="dokan-form-control product_price" name="_regular_price" placeholder="&ndash;" value="<?php echo get_post_meta( $product->get_id(), '_regular_price', true ); ?>">
								 <!--<label style="font-weight: 500; display: block; text-align: center;" for="uf_ajax_price_<?php echo $product->get_slug(); ?>"><?php echo $product->get_price_suffix(); ?></label>-->

                                 <!--<span class="dokan-input-group-addon" style="padding: 6px 4px;"><label style="font-weight: 500;" for="uf_ajax_price_<?php /*echo $product->get_slug(); */?>"><?php /*echo $product->get_price_suffix(); */?></label></span>-->

                             </div>

<label style="font-weight: 500; display: block; text-align: center;" for="uf_ajax_price_<?php echo $product->get_slug(); ?>"><?php echo $product->get_price_suffix(); ?></label>

                            <span style="display: none; font-size: 10.5px; color: #5cb85c; font-weight: 900;" id="uf_price_change_result_<?php echo $product->get_id(); ?>">price has been changed</span>



                        </td>

            </div>



            </td>

            </tr>



            <?php } ?>



            <?php } else { ?>

                <tr>

                    <td colspan="7"><?php _e( 'No product found', 'dokan-lite' ); ?></td>

                </tr>

            <?php } ?>



            </tbody>



            </table>



    <!--<form method="post" id="store-form"  action="" class="dokan-form-horizontal">

        <div class="dokan-form-group dokan-text-left">
            <label class="dokan-w3 dokan-control-label dokan-control-label-left" for="stick_location"><?php /*_e( 'Location Settings', 'dokan' ); */?></label>
            <div class="dokan-w9 dokan-text-left">
                <div class="checkbox">
                    <label>
                        <input type="hidden" name="stick_location" value="no" />
                        <input class="checkbox-style" type="checkbox" name="stick_location" id="stick_location" value="yes" <?php /*checked( $stick_location, 'yes' ); */?>>
                        <span class="checkbox-custom"></span>
                        <span class="label "><?php /*_e( 'Enable real time location', 'dokan' ); */?></span>
                    </label>
                </div>
            </div>
        </div>

        <div class="dokan-form-group">
            <div class="dokan-w4 ajax_prev dokan-text-left" style="margin-left:24%;">
                <input type="submit" name="dokan_update_stick_location" class="dokan-btn dokan-btn-danger dokan-btn-theme dokan-btn-lg" value="<?php /*esc_attr_e( 'Update Settings', 'dokan' ); */?>">
            </div>
        </div>
    </form>-->

        </article>



    </div>

    <?php

    wp_reset_postdata();



    $pagenum      = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

    $base_url = dokan_get_navigation_url('products');



    if ( $product_query->max_num_pages > 1 ) {

        echo '<div class="pagination-wrap">';

        $page_links = paginate_links( array(

            'current'   => $pagenum,

            'total'     => $product_query->max_num_pages,

            'base'      => $base_url. '%_%',

            'format'    => '?pagenum=%#%',

            'add_args'  => false,

            'type'      => 'array',

            'prev_text' => __( '&laquo; Previous', 'dokan-lite' ),

            'next_text' => __( 'Next &raquo;', 'dokan-lite' )

        ) );



        echo '<ul class="pagination"><li>';

        echo join("</li>\n\t<li>", $page_links);

        echo "</li>\n</ul>\n";

        echo '</div>';

    }

    ?>

    </article>



    <?php



    /**

     *  dokan_dashboard_content_before hook

     *

     *  @hooked get_dashboard_side_navigation

     *

     *  @since 2.4

     */

    do_action( 'dokan_dashboard_content_inside_after' );

    do_action( 'dokan_after_listing_product' );

    ?>



</div><!-- #primary .content-area -->



<?php



/**

 *  dokan_dashboard_content_after hook

 *

 *  @since 2.4

 */

do_action( 'dokan_dashboard_content_after' );

?>



</div><!-- .dokan-dashboard-wrap --
