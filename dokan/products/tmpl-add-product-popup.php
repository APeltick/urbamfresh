<script type="text/html" id="tmpl-dokan-add-new-product">
    <div id="dokan-add-new-product-popup" class="white-popup dokan-add-new-product-popup"><h2><i
                    class="fa fa-briefcase">&nbsp;</i>&nbsp;<?php _e('Add New Product', 'dokan'); ?></h2>
        <form action="../../inc/jcrop/demos/crop.php" method="post" onsubmit="return checkCoords();" id="dokan-add-new-product-form" >

            <input type="hidden" id="x" name="x" />
            <input type="hidden" id="y" name="y" />
            <input type="hidden" id="w" name="w" />
            <input type="hidden" id="h" name="h" />
            <input type="hidden" id="cropbox-width" name="cropbox-width" />

            <div class="product-form-container">
                <div class="content-half-part dokan-feat-image-content">
                    <div class="dokan-feat-image-upload">
                        <?php $wrap_class = ' dokan-hide';
                        $instruction_class = '';
                        $single_image_id = 0; ?>
                        <div id="uf-intruction-inside" class="instruction-inside<?php echo $instruction_class; ?>"><input type="hidden"
                                                                                                                          name="single_image_id"
                                                                                                                          class="dokan-feat-image-id"
                                                                                                                          value="<?php echo $single_image_id; ?>">
                            <input type="hidden"
                                   name="uf_image_gallery_ids"
                                   value="">

                            <!--<a href="#" class="dokan-feat-image-btn btn btn-sm"
                               style="padding: 100px 0 100px 0; background: inherit; display: block;"><i
                                        class="fa fa-cloud-upload"></i><?php /*_e('Upload a product cover image', 'dokan'); */?>
                            </a>-->



                            <div id="uf-product-pict-container">

                                <a id="uf-product-pict" class="btn btn-sm" style="padding: 100px 0 100px 0; background: inherit; display: block;">
                                    <i style="display: block; font-size: 80px; color: #DFDFDF; text-align: center; margin: auto;" class="fa fa-cloud-upload uf-loading-spin"></i><?php _e('Upload a product cover image', 'dokan'); ?>
                                </a>

                            </div>


                        </div>
                        <div class="image-wrap<?php echo $wrap_class; ?>">
                            <img id="cropbox" height="" width="" src="" alt=""/>
                        </div>
                    </div>
                </div>
                <div style="text-align: center;">
                    <a id="uf-remove-image" class="close dokan-remove-feat-image dokan-btn dokan-btn-theme">Delete image</a>
                    <div id="uf-crop-save" class="dokan-btn dokan-btn-theme">Save crop</div>
                    <div id="uf-crop-toggle" class="dokan-btn dokan-btn-theme" onclick="ufCropInit();">Crop image</div>
                    <div id="uf-crop-destructor" class="dokan-btn dokan-btn-theme" onclick="ufCropDestruct();">Cancel crop</div>
                </div>
                <div style="text-align: center;">
                    <span id="uf-crop-error">You should create crop area on the image before saving</span>
                </div>
                <div id="uf-featured-thumbnails-wrapper">
                    <div class="uf-featured-thumbnails-title">Uploaded images: </div>
                    <div id="uf-featured-thumbnails-container"> <!-- thumbnails container -->
                        <div id="uf-add-new-image-small-container" class="uf-featured-thumbnail" style="padding-top: 0;">
                            <div id="uf-add-new-image-small"> <!-- add new picture small button -->
                                <img style="opacity: .3" src="<?php echo get_stylesheet_directory_uri()?>/img/uf-add-new-image.png" />
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 15px;" id="uf-clearfix" class="clearfix"></div>
                    </div>
                </div>
                <div class="content-half-part dokan-product-field-content">
                    <div class="dokan-form-group"><input type="text" class="dokan-form-control" name="post_title"
                                                         placeholder="<?php _e('Product name..', 'dokan'); ?>"></div>
                    <div class="dokan-clearfix">
                        <div class="dokan-form-group dokan-clearfix dokan-price-container"><label for="_regular_price"
                                                                                                  class="form-label"><?php _e('Price', 'dokan'); ?></label>
                            <div class="dokan-input-group"><span
                                        class="dokan-input-group-addon"><?php echo get_woocommerce_currency_symbol(); ?></span>
                                <input id="uf-regular-price" type="number" class="dokan-form-control" name="_regular_price" placeholder="0.00">
                            </div>
                        </div>
                        <div class="dokan-form-group dokan-clearfix"><label for="_units"
                                                                            class="form-label"><?php _e('Units', 'dokan'); ?></label>
                            <div class="dokan-input-group" style="width: 100%;">
                                <p><select id="uf-units" class="dokan-form-control" name="_units" style="width: 50%;">
                                        <option value="each">each</option>
                                        <option value="lb">lb</option>
                                        <option value="custom">custom</option>
                                    </select> <input id="uf-custom-input" type="text" class="dokan-form-control"
                                                     name="custom_units" placeholder=""
                                                     style="display: none; float: right; width: 49%; margin-left: 1%;">
                                </p>
                            </div>
                        </div>
                        <div class="dokan-hide sale-schedule-container sale_price_dates_fields dokan-clearfix dokan-form-group">
                            <div class="content-half-part from">
                                <div class="dokan-input-group"><span
                                            class="dokan-input-group-addon"><?php _e('From', 'dokan'); ?></span> <input
                                            type="text" name="_sale_price_dates_from"
                                            class="dokan-form-control datepicker sale_price_dates_from" value=""
                                            maxlength="10"
                                            pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
                                            placeholder="YYYY-MM-DD"></div>
                            </div>
                            <div class="content-half-part to">
                                <div class="dokan-input-group"><span
                                            class="dokan-input-group-addon"><?php _e('To', 'dokan'); ?></span> <input
                                            type="text" name="_sale_price_dates_to"
                                            class="dokan-form-control datepicker sale_price_dates_to" value=""
                                            maxlength="10"
                                            pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
                                            placeholder="YYYY-MM-DD"></div>
                            </div>
                        </div><!-- .sale-schedule-container -->                    </div>
                </div>
                <div class="dokan-clearfix"></div>
                <div class="product-full-container">                    <?php if (dokan_get_option('product_category_style', 'dokan_selling', 'single') == 'single'): ?>
                        <label for="product_cat" class="form-label"><?php _e('Category', 'dokan'); ?></label>
                        <div class="dokan-form-group">                            <?php $product_cat = 14;
                            $category_args = array('hierarchical' => 1, 'hide_empty' => 0, 'name' => 'product_cat', 'id' => 'product_cat', 'taxonomy' => 'product_cat', 'title_li' => '', 'class' => 'product_cat dokan-form-control', 'exclude' => '', 'selected' => $product_cat,);
                            wp_dropdown_categories(apply_filters('dokan_product_cat_dropdown_args', $category_args)); ?>                        </div>                    <?php elseif (dokan_get_option('product_category_style', 'dokan_selling', 'single') == 'multiple'): ?>
                        <div class="dokan-form-group">                            <?php $term = array();
                            include_once DOKAN_LIB_DIR . '/class.taxonomy-walker.php';
                            $drop_down_category = wp_dropdown_categories(array('show_option_none' => __('', 'dokan'), 'hierarchical' => 1, 'hide_empty' => 0, 'name' => 'product_cat[]', 'id' => 'product_cat', 'taxonomy' => 'product_cat', 'title_li' => '', 'class' => 'product_cat dokan-form-control dokan-select2', 'exclude' => '', 'selected' => $term, 'echo' => 0, 'walker' => new DokanTaxonomyWalker()));
                            echo str_replace('<select', '<select data-placeholder="' . __('Select product category', 'dokan') . '" multiple="multiple" ', $drop_down_category); ?>                        </div>                    <?php endif; ?>
                    <div class="dokan-form-group"><textarea name="post_excerpt" id="" class="dokan-form-control"
                                                            rows="5"
                                                            placeholder="<?php _e('Enter some short description about this product...') ?>"></textarea>
                    </div>
                </div>
            </div>
            <div class="product-container-footer"><span class="dokan-show-add-product-error"></span> <span
                        class="dokan-spinner dokan-add-new-product-spinner dokan-hide"></span> <input type="submit"
                                                                                                      id="dokan-create-and-add-new-product-btn"
                                                                                                      class="dokan-btn dokan-btn-theme dokan-btn-lg"
                                                                                                      data-btn_id="create_and_new"
                                                                                                      value="<?php _e('Create product', 'dokan') ?>">
            </div>
        </form>
    </div></script>

<script type="text/javascript">

    var uf_product_images = {};

    (function ($) {
        $(document).ready((function () {

            var uf_price_regexp = new RegExp('^\\d{0,9}[\\.,]{0,1}\\d{0,2}$');
            $('body').on('input', '#uf-regular-price', function () {
                var val = $(this).val(), match = val.match(uf_price_regexp);
                if( val == '' ) {
                    //$(this).val( val );
                }
                if ( val != '' && (match == null || typeof match[0] == 'undefined' || match[0] !== val) ) {
                    $(this).val( $(this).data('previous_value') );
                }
            }).on('keydown', '#uf-regular-price', function () {
                var val =  $(this).val();
                $(this).data('previous_value', val);
            });

            //$('#uf-regular-price').inputmask("9{7}.9{2}");

            $('body').on('click', '#dokan-create-and-add-new-product-btn', function () {
                $('input[name="uf_image_gallery_ids"]').val(Object.keys(uf_product_images).join(','));
            });

            $('body').on('change', '#uf-units', function () {
                if ($(this).val() === 'custom') {
                    $('#uf-custom-input').show();
                } else {
                    $('#uf-custom-input').hide();
                }
            });

            $('body').on('focus', '#dokan-add-new-product-popup textarea, #dokan-add-new-product-popup input[type=password], #dokan-add-new-product-popup input[type=text], #dokan-add-new-product-popup input[type=email]', function( event ) {
                var $scrollHeight = $(event.target).position().top;
                if ( $scrollHeight > ($('.product-form-container').outerHeight())/2) {
                    $('.product-form-container').scrollTop($('.product-form-container').scrollTop() + ($scrollHeight - 80)/2);
                }
            });

            $('body').on('click', '.uf-featured-thumbnail-img', function (e) {
                // Change active image on thumbnail click
                var $img_url = $(e.target).css('background-image');
                //console.log($img_url);
                $img_url = $img_url.replace('url(','').replace(')','').replace(/\"/gi, "");
                //console.log($img_url);
                $('#cropbox').attr('src', $img_url);
                $(e.target).siblings().removeClass('uf-selected-image');
                $(e.target).addClass('uf-selected-image');
            });

            // Save crop parameters in 'uf_product_images' object
            $('body').on('click', '#uf-crop-save', function () {
                var currentImageId = $('.uf-selected-image').data("image-id");
                //uf_product_images[currentImageId]['x'] = $('#x').val();
                //uf_product_images[currentImageId]['y'] = $('#y').val();
                //uf_product_images[currentImageId]['w'] = $('#w').val();
                //uf_product_images[currentImageId]['h'] = $('#h').val();
                //uf_product_images[currentImageId]['cropbox-width'] = $('#cropbox-width').val();
                if ( $('#x').val() === '' || $('#y').val() === '' || $('#w').val() === '' || $('#h').val() === '' ) {
                    //console.log('You should create crop area by dragging a rectangle on the image before saving');
                    $('#uf-crop-error').fadeIn().css("display","inline-block");
                    return false;
                }
                $('#uf-crop-error').fadeOut();
                var $instruction = $('#uf-intruction-inside');
                var $wrap = $instruction.siblings('.image-wrap');
                $wrap.addClass('dokan-hide');
                $instruction.removeClass('dokan-hide');
                ufCropDestruct();
                $('#uf-crop-toggle').fadeOut();
                $('#uf-remove-image').fadeOut();
                $('#uf-crop-save').fadeOut();
                $('#uf-crop-destructor').fadeOut();
                $('.uf-loading-spin').removeClass('fa-cloud-upload').addClass('fa-refresh fa-3x fa-spin fa-fw');
                $.ajax({
                    method: "POST",
                    url: "<?php echo admin_url('admin-ajax.php'); ?>",
                    dataType: "json",
                    data: {
                        action: 'uf_ajax_crop_handler',
                        single_image_id: currentImageId,
                        cropbox_width: $('#cropbox-width').val(),
                        x : $('#x').val(),
                        y : $('#y').val(),
                        w : $('#w').val(),
                        h : $('#h').val()
                    },
                    success: function (data) {
                        if ( data.success === true ) {
                            uf_product_images[currentImageId] = data.data;
                            $('.uf-selected-image').css('background-image', 'url(' + uf_product_images[currentImageId] + ')');
                            $('#cropbox').attr('src', uf_product_images[currentImageId]);
                            //$('#cropbox').css('width', '100%', 'important');
                            $instruction.addClass('dokan-hide');
                            $wrap.removeClass('dokan-hide');
                            $('#uf-crop-toggle').fadeIn().css("display","inline-block");
                            $('#uf-remove-image').fadeIn().css("display","inline-block");
                            $('.uf-loading-spin').removeClass('fa-refresh fa-3x fa-spin fa-fw').addClass('fa-cloud-upload');
                        } else {
                            console.log("ajax-error: something went wrong");
                            $instruction.addClass('dokan-hide');
                            $wrap.removeClass('dokan-hide');
                            $('#uf-crop-toggle').fadeIn().css("display","inline-block");
                            $('#uf-remove-image').fadeIn().css("display","inline-block");
                            $('.uf-loading-spin').removeClass('fa-refresh fa-3x fa-spin fa-fw').addClass('fa-cloud-upload');
                        }
                    },

                    error: function () {
                        console.log("ajax-error: can't crop image");
                        $instruction.addClass('dokan-hide');
                        $wrap.removeClass('dokan-hide');
                        $('#uf-crop-toggle').fadeIn().css("display","inline-block");
                        $('#uf-remove-image').fadeIn().css("display","inline-block");
                        $('.uf-loading-spin').removeClass('fa-refresh fa-3x fa-spin fa-fw').addClass('fa-cloud-upload');
                    }

                });
            });

            // IMAGE UPLOADING

            $('body').on('open-product-popup', function () {

                $('#uf-product-pict, #uf-add-new-image-small').each(function() {
                    var id = $(this).prop('id');
                    var $obj = $(this);
                    var plup_settings = {
                        runtimes : 'html5,flash,silverlight,html4',
                        browse_button: id,
                        container: $obj.parent().get(0),
                        url: "<?php echo admin_url( 'async-upload.php', 'relative' )?>",
                        resize: {
                            width: 650,
                            height: 650,
                            preserve_headers: false
                        },
                        flash_swf_url : "<?php echo includes_url( 'js/plupload/plupload.flash.swf' )?>",
                        silverlight_xap_url : "<?php echo includes_url( 'js/plupload/plupload.silverlight.xap' )?>",
                        filters: {
                            mime_types: [{
                                title: 'Allowed Files',
                                extensions: '*'
                            }]
                        },
                        max_files: 0,
                        file_data_name : "async-upload",
                        max_file_size : "<?php echo wp_max_upload_size() . 'b' ?>",
                        multipart_params : {
                            action : 'upload-attachment',
                            _wpnonce : '<?php echo wp_create_nonce( 'media-form' ) ?>'
                        },
                        init : {
                            FilesAdded: function(up, files) {
                                up.start();

                                $('.uf-featured-thumbnail-img').removeClass('uf-selected-image');

                                var $instruction = $('#uf-intruction-inside');
                                var $wrap = $instruction.siblings('.image-wrap');

                                //$instruction.find('input.dokan-feat-image-id').val('0');
                                $wrap.addClass('dokan-hide');
                                $instruction.removeClass('dokan-hide');

                                $('#uf-crop-toggle').fadeOut();
                                $('#uf-remove-image').fadeOut();
                            },
                            UploadProgress: function (up, file) {
                                $('.uf-loading-spin').removeClass('fa-cloud-upload').addClass('fa-refresh fa-3x fa-spin fa-fw');
                            },
                            FileUploaded: function (up, files, response) {
                                response = JSON.parse( response.response );
                                // set the image hidden id
                                $('input.dokan-feat-image-id[name="single_image_id"]').val(response.data.id);
                                uf_product_images[response.data.id] = {id:response.data.id};

                                // set the image
                                var $instruction = $('#uf-intruction-inside');
                                var $wrap = $instruction.siblings('.image-wrap');

                                //var $wrap = $('.dokan-feat-image-upload .image-wrap');
                                var img = new Image();
                                img.onload = function() {
                                    $wrap.find('img').attr('src', response.data.url);
                                    $instruction.addClass('dokan-hide');
                                    $wrap.removeClass('dokan-hide');
                                    $('<div data-image-id="' + response.data.id + '" class="uf-featured-thumbnail uf-featured-thumbnail-img uf-selected-image"></div>').insertBefore("#uf-clearfix").css("background", "url('" + response.data.url + "')");
                                    $('#uf-crop-toggle').fadeIn().css("display","inline-block");
                                    $('#uf-remove-image').fadeIn().css("display","inline-block");
                                    $('#uf-featured-thumbnails-wrapper').fadeIn();
                                    $('.uf-loading-spin').removeClass('fa-refresh fa-3x fa-spin fa-fw').addClass('fa-cloud-upload');
                                };
                                img.src = response.data.url;
                            }
                        }
                    };

                    uploader = new plupload.Uploader( plup_settings );
                    uploader.init();

                });

            });

        }));
    })(jQuery);

</script>

<script src="<?php echo get_stylesheet_directory_uri() . '/inc/jcrop/js/jquery.Jcrop.js' ?>" ></script>
<!--<link rel="stylesheet" href="<?php /*echo get_stylesheet_directory_uri() . '/inc/jcrop/demo_files/main.css' */?>" type="text/css" />
<link rel="stylesheet" href="<?php /*echo get_stylesheet_directory_uri() . '/inc/jcrop/demo_files/demos.css' */?>" type="text/css" />-->
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/inc/jcrop/css/jquery.Jcrop.css' ?>" type="text/css" />

<script type="text/javascript">

    var jcrop_api;

    function ufCropInit() {
        jQuery(function(){
            jQuery('#x').val('');
            jQuery('#y').val('');
            jQuery('#w').val('');
            jQuery('#h').val('');
            jQuery('#cropbox').Jcrop({
                aspectRatio: 1,
                onSelect: updateCoords
            }, function(){
                jcrop_api = this;
            });
        });

        function updateCoords(c) {
            jQuery('#x').val(c.x);
            jQuery('#y').val(c.y);
            jQuery('#w').val(c.w);
            jQuery('#h').val(c.h);
            jQuery('#cropbox-width').val(jQuery('#cropbox').width());
        }

        function checkCoords() {
            if (parseInt(jQuery('#w').val())) return true;
            alert('Please select a crop region or cancel cropping then press "Save crop".');
            return false;
        }
        jQuery('#uf-crop-toggle').hide();
        jQuery('#uf-remove-image').hide();
        jQuery('#uf-crop-save').show().css("display","inline-block");
        jQuery('#uf-crop-destructor').show().css("display","inline-block");
    }

    function ufCropDestruct() {
        jcrop_api.destroy();
        jQuery('#uf-crop-error').fadeOut();
        jQuery('#uf-crop-destructor').hide();
        jQuery('#uf-crop-save').hide();
        jQuery('#uf-remove-image').show().css("display","inline-block");
        jQuery('#uf-crop-toggle').show().css("display","inline-block");
        return false;
    }

</script>
