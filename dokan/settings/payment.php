<?php
do_action('dokan_payment_settings_before_form', $current_user, $profile_info); ?>
<form method="post" id="payment-form" action="" class="dokan-form-horizontal custom-payment-form-style">
    <?php
    wp_nonce_field('dokan_payment_settings_nonce');
    foreach ($methods as $method_key) {
        $method = dokan_withdraw_get_method($method_key);
        $enable = isset($profile_info['payment'][$method_key]['enable']) ? $profile_info['payment'][$method_key]['enable'] : false;
        ?>
        <fieldset class="payment-field-<?php echo $method_key; ?>">
            <div class="dokan-form-group">
                <label class="dokan-w3 dokan-control-label">
                    <input type="checkbox" name="settings[<?php echo $method_key ?>][enable]" class="enable_payment_method checkbox-style" value="1" <?php checked($enable); ?> />

                    <span class="checkbox-custom"></span>
                    <span class="label"><?php echo $method['title'] ?></span>
                </label>
                <div class="dokan-w6">
                    <?php if ( is_callable( $method['callback'] ) ) {
                        call_user_func($method['callback'], $profile_info);
                    } ?>
                </div> <!-- .dokan-w6 -->
            </div>
        </fieldset>
    <?php }
    do_action('dokan_payment_settings_form_bottom', $current_user, $profile_info); ?>
    <div class="dokan-form-group">
        <div class="dokan-w4 ajax_prev dokan-text-left" style="margin-left:24%;">
            <input type="submit" name="dokan_update_payment_settings" class="dokan-btn dokan-btn-danger dokan-btn-theme dokan-btn-lg" value="<?php esc_attr_e('Update Settings', 'dokan'); ?>">
        </div>
    </div>
</form>
<?php do_action('dokan_payment_settings_after_form', $current_user, $profile_info); ?>
<script type="text/javascript">
    (function ($) {
        $('.enable_payment_method').on('click', function () {
            $settings_block = $(this).parent().siblings('.dokan-w6');
            if ($(this).is(':checked')) {
                $settings_block.show();
            } else {
                $settings_block.hide();
            }
        }).each(function () {
            $(this).triggerHandler('click');
        });
    })(jQuery);
</script>
