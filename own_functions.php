<?php
    remove_action( 'woocommerce_thankyou', 'woocommerce_order_details_table', 10 );
    if ( ! function_exists( 'woocommerce_order_details_table_own' ) ) {

    	/**
    	 * Displays order details in a table.
    	 *
    	 * @param mixed $order_id
    	 * @subpackage	Orders
    	 */
    	function woocommerce_order_details_table_own( $order_id ) {
    		if ( ! $order_id ) {
    			return;
    		}

    		wc_get_template( 'order/order-details-own.php', array(
    			'order_id' => $order_id,
    		) );
    	}
    }
    add_action( 'woocommerce_thankyou_own', 'woocommerce_order_details_table_own', 10 );

    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

    if ( ! function_exists( 'woocommerce_checkout_coupon_form_own' ) ) {

    	/**
    	 * Output the Coupon form for the checkout.
    	 *
    	 * @subpackage	Checkout
    	 */
    	function woocommerce_checkout_coupon_form_own() {
    		wc_get_template( 'checkout/form-coupon.php', array( 'checkout' => WC()->checkout() ) );
    	}
    }
    add_action( 'woocommerce_own_coupon_form', 'woocommerce_checkout_coupon_form_own', 10 );
 ?>
