<?php

define( 'MAP_PAGE_ID', 72 );
define( 'SLIDER_PAGE_ID', 2 );

require_once get_theme_file_path() . '/App/autoload.php';
\App\ApplicationCore::instance();

add_action('wp_loaded', 'my_func');

function my_func() {
    if ( $_SERVER['REMOTE_ADDR'] == '178.165.51.207' ) {
//        require_once(ABSPATH.'wp-admin/includes/user.php');
//        wp_delete_user(270);
    }
}


require_once 'class.coupon.php';
require_once 'class.sms.php';
require_once 'class.messages.php';
//require_once 'class.wc-currency-per-product.php';
require_once 'own_functions.php';

add_action('template_redirect', 'remove_admin_bar', 9999);
function remove_admin_bar() {
      show_admin_bar(false);
}

add_filter('show_admin_bar', 'cc_show_admin_bar', 9999);
function cc_show_admin_bar($t) {
      return false;
}

remove_filter('the_content', 'wpautop');

function child_theme_style() {

    global $wp;
    wp_enqueue_style('storefront-child-style', get_template_directory_uri() . '/style.css');

    if ( isset( $wp->query_vars['settings'] ) && $wp->query_vars['settings'] == 'store' ) {

        wp_enqueue_style('storefront-timepicker-css', get_stylesheet_directory_uri() . '/assets/css/jquery.timepicker.css');
        wp_enqueue_script('storefront-timepicker-js', get_stylesheet_directory_uri() . '/assets/js/jquery.timepicker.js');

        wp_enqueue_script('uf_jquery_phone_js', get_stylesheet_directory_uri() . '/js/jquery.phone/intlTelInput.min.js', array('jquery') );
        wp_enqueue_script('uf_store_settings', get_stylesheet_directory_uri() . '/js/store_settings.js', array('jquery', 'uf_jquery_phone_js') );

        wp_localize_script('uf_store_settings', 'uf_store_settings_var', array(
            'uploader' => array(
                'async_upload' => admin_url( 'async-upload.php', 'relative' ),
                'flash' => includes_url( 'js/plupload/plupload.flash.swf' ),
                'xap' => includes_url( 'js/plupload/plupload.silverlight.xap' ),
                'max_upload_size' => wp_max_upload_size() . 'b',
                'nonce' => wp_create_nonce( 'media-form' )
            ),
            'tel_util_script' => get_stylesheet_directory_uri() . '/js/jquery.phone/utils.js'
        ));

        wp_enqueue_style('uf_jquery_phone_css', get_stylesheet_directory_uri() . '/assets/css/intlTelInput.css' );
    }

    if( is_product() ) {
        wp_enqueue_script('uf_dotdotdot', get_stylesheet_directory_uri() . '/js/jquery.dotdotdot.js');
    }
}

add_action( 'wp_enqueue_scripts', 'child_theme_style', 999 );

function wc_admin_custom_css() {
    wp_enqueue_style('wc_custom_css_', get_stylesheet_directory_uri() . '/assets/css/wc_admin_custom.css');

}
add_action('admin_print_styles', 'wc_admin_custom_css');

//add_action('admin_head', 'wc_admin_custom_css');

add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

// remove_action('woocommerce_single_product_summary');
//     ,'woocommerce_template_single_add_to_cart',100);
// remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
add_action('woocommerce_single_product_summary','dz_auther_name',6);
add_action('woocommerce_single_product_summary','dz_auther_pic',4);

remove_action('woocommerce_after_single_product_summary','woocommerce_output_product_data_tabs',10);
add_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);

function dz_auther_pic()
{
   $author_id = get_the_author_ID();
   echo '<div class="profile-img profile-img-circle">';
   echo get_avatar($author_id, 50);
   echo  '</div>';

}

function dz_auther_name(){
    global $post;
    echo 'By <span class="authorname" >';
    $store_info = dokan_get_store_info( $post->post_author );
    echo $store_info['store_name'];
    echo '</span>';
}

function check_gps_track( $user_id = '' ) {
    if( !$user_id ) {
	    $user_id = get_current_user_id();
    }
    $seller_settings = get_user_meta($user_id, 'dokan_profile_settings', true);
    $location_type = !empty( $seller_settings['location_type'] ) ? $seller_settings['location_type'] : 'store_hours';
    $gps = !empty( $seller_settings['gps'] ) ? $seller_settings['gps'] : '';
    if( !$gps ) return false;
    if( $location_type == 'stick' ) {
	    return true;
    } else {
        return check_if_store_open_now();
    }
}

function check_if_store_open_now( $user_id = '' ) {
    if( !$user_id ) {
	    $user_id = get_current_user_id();
    }
	$seller_settings = get_user_meta($user_id, 'dokan_profile_settings', true);//getting settings of curr store/user
	$time_zone = $seller_settings['timezone'] ? $seller_settings['timezone'] : 0;//SETTING TIMEZONE
	$time = time() + $time_zone*3600;//this is current time with timezone diff;
	$day = substr(gmdate( 'l' , $time), 0, 2);//here we have day code like: Mo, We, Th to compare it with array

    if(in_array( $day, $seller_settings['store_days'] ) ) {//checking if today is a working day

		$time_from = str_replace(array( '%am%', '%pm%' ),array( ' AM', ' PM' ), $seller_settings['store_from']);
		$time_to = str_replace(array( '%am%', '%pm%' ),array( ' AM', ' PM' ), $seller_settings['store_to']);

		$time_from = gmdate("H:i", strtotime( $time_from ));//HERE WE HAVE normal 24h time like: 12:30
		$time_to = gmdate("H:i", strtotime( $time_to ));

		$date1 = DateTime::createFromFormat('H:i', gmdate("H:i", $time));
		$date2 = DateTime::createFromFormat('H:i', $time_from);
		$date3 = DateTime::createFromFormat('H:i', $time_to);

		if ($date1 > $date2 && $date1 < $date3){//this if has now working store only
			return true;
		}else {
			return false; //skip this shop, it's not working this time
		}
	}else {
		return false;//skip this shop, it's not working today
	}
}

function uf_set_cookies ($latitude, $longitude, $city) {
    if( !empty( $_COOKIE['latitude'] ) && !empty( $_COOKIE['longitude'] ) &&
        $_COOKIE['latitude'] == $latitude && $_COOKIE['longitude'] == $longitude ) return true;

    /*------PREVENT GETTING GPS LOCATION in not working hours------*/
	if( is_user_logged_in() && !check_gps_track() ) {
        return false;
	}
	/*------PREVENT GETTING GPS LOCATION in not working hours------*/

	setcookie('latitude', $latitude, time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
    setcookie('longitude', $longitude, time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );

	if ( is_user_logged_in() ) {
	    if( get_user_meta( get_current_user_id(), 'uf_latitude', true ) == $latitude && get_user_meta( get_current_user_id(), 'uf_longitude', true ) == $longitude )
	        return false;
    }

    $api_key = dokan_get_option( 'gmap_api_key', 'dokan_general', false );
    $response = wp_remote_get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&key=' . $api_key );
    if( is_wp_error( $response ) || empty( $response['body'] ) ) {
        $gm_data = json_decode( $response['body'], true );
    } else {
        $gm_data = array();
    }

    if( isset( $gm_data['status'] ) && $gm_data['status'] == 'OK' && !empty( $gm_data['results'][0]['formatted_address'] ) ) {
        setcookie('address', $gm_data['results'][0]['formatted_address'], time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
    }

    if ( is_user_logged_in() ) {
	    update_user_meta(get_current_user_id(), 'uf_latitude', $latitude);
        update_user_meta(get_current_user_id(), 'uf_longitude', $longitude);
    }
    $seller_settings = get_user_meta(get_current_user_id(), 'dokan_profile_settings', true);
    if( isset( $gm_data['status'] ) && $gm_data['status'] == 'OK' &&
        isset( $gm_data['results'][0]['address_components'] ) &&
        is_array( $gm_data['results'][0]['address_components'] ) ) {

        $data = array(
            'city' => isset( $city ) ? $city : ''
        );

        foreach( $gm_data['results'][0]['address_components'] as $component ) {
            if( isset( $component['types'] ) && in_array( 'country', $component['types'] ) && isset( $component['short_name'] ) ) {
                $data['country_code'] = $component['short_name'];
                continue;
            }

            if( isset( $component['types'] ) && in_array( 'postal_code', $component['types'] ) && isset( $component['short_name'] ) ) {
                $data['zip'] = $component['short_name'];
                continue;
            }

            if( isset( $component['types'] ) && in_array( 'administrative_area_level_1', $component['types'] ) && isset( $component['short_name'] ) ) {
                $data['state'] = $component['long_name'];
                $data['state_code'] = $component['short_name'];
                continue;
            }

            if( isset( $component['types'] ) && in_array( 'route', $component['types'] ) && isset( $component['short_name'] ) ) {
                $data['street_1'] = $component['short_name'];
                continue;
            }

            if( isset( $component['types'] ) && in_array( 'street_number', $component['types'] ) && isset( $component['short_name'] ) ) {
                $data['street_2'] = $component['short_name'];
                continue;
            }
        }

        setcookie('uf_location', serialize( $data ), time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );

        $update_address = false;
        if ( is_user_logged_in() && isset( $seller_settings["gps"] ) && $seller_settings["gps"] == '1' ) {
            if( isset( $seller_settings["location_type"] ) && $seller_settings["location_type"] == 'stick' ) {
                $update_address = true;
            } elseif ( isset( $seller_settings["location_type"] ) && $seller_settings["location_type"] == 'stick_store_hours' && check_if_store_open_now() ) {
                $update_address = true;
            }
        }

        if( $update_address ) {
            $seller_settings["address"]["street_1"] = isset($data['street_1']) ? $data['street_1'] : '';
            $seller_settings["address"]["street_2"] = isset($data['street_2']) ? $data['street_2'] : '';
            $seller_settings["address"]["city"]     = isset($data['city']) ? $data['city'] : '';
            $seller_settings["address"]["zip"]      = isset($data['zip']) ? $data['zip'] : '';
            $seller_settings["address"]["country"]  = isset($data['country_code']) ? $data['country_code'] : '';
            $seller_settings["address"]["state"]    = isset($data['state']) ? $data['state'] : '';

            update_user_meta( get_current_user_id(), 'dokan_profile_settings', $seller_settings );
        }

    } else {
        if( is_user_logged_in() && isset( $seller_settings["address"] ) ) {
            unset($seller_settings["address"]);
            update_user_meta( get_current_user_id(), 'dokan_profile_settings', $seller_settings );
        }
    }
    return('');
}

function cc_plugins_loaded() {
    if ( !( isset( $_COOKIE['uf_flag'] ) && $_COOKIE['uf_flag'] == '1' ) &&
        !isset( $_COOKIE['latitude'] ) && !isset( $_COOKIE['longitude'] ) ) {

        $uf_record = get_coordinates_by_ip();
        uf_set_cookies($uf_record['lat'], $uf_record['lng'], $uf_record['city']);
        // var_dump - delete next line
        setcookie('location_source', 'from ip', time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
        wp_redirect( (is_ssl() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] );
    }

    if( is_user_logged_in() ) {
        $user_id = get_current_user_id();
        $latitude = get_user_meta( $user_id, 'uf_latitude', true );
        $longitude = get_user_meta( $user_id, 'uf_longitude', true );



        if( $latitude != '' && $longitude != '' &&
            isset( $_COOKIE['latitude'] ) && $_COOKIE['latitude'] != '' &&
            isset( $_COOKIE['longitude'] ) && $_COOKIE['longitude'] != '' && need_update_location() ) {
            if( $_COOKIE['latitude'] != $latitude || $_COOKIE['longitude'] != $longitude ) {
                update_user_meta( $user_id, 'uf_latitude', $_COOKIE['latitude'] );
                update_user_meta( $user_id, 'uf_longitude', $_COOKIE['longitude'] );
            }
        }

    }
}

if( strpos( $_SERVER['PHP_SELF'], 'location.php' ) === false ) {
    if ($_SERVER['REMOTE_ADDR'] != '178.165.51.207' || $_SERVER['REMOTE_ADDR'] != '37.57.0.213') {
        add_action('init', 'cc_plugins_loaded');
    }
}

function need_update_location( $user_id = '' ) {
    $user_id = empty( $user_id ) ? get_current_user_id() : $user_id;
    $store_info = dokan_get_store_info( $user_id );

    $need_update = false;
    if ( is_user_logged_in() && isset( $store_info["gps"] ) && $store_info["gps"] == '1' ) {
        if( isset( $store_info["location_type"] ) && $store_info["location_type"] == 'stick' ) {
            $need_update = true;
        } elseif ( isset( $store_info["location_type"] ) && $store_info["location_type"] == 'stick_store_hours' && check_if_store_open_now( $user_id ) ) {
            $need_update = true;
        }
    }
    return $need_update;
}

function cc_dokan_get_dashboard_nav( $nav ) {

    $nav['products']['title'] = __( 'Manage Products', 'dokan');
    $nav['orders']['title'] = __( 'Manage Orders', 'dokan');
    $nav['settings']['title'] = __( 'Store Settings', 'dokan');
    $nav['account_details'] = array(
        'title' => __( 'Account Details', 'dokan' ),
        'icon' => '<i class="fa fa-user"></i>',
        'url' => dokan_get_navigation_url( 'edit-account' ),
        'pos' => 250
    );

    $nav['account_details'] = array(
        'title' => __( 'Payment', 'dokan' ),
        'icon' => '<i class="fa fa-credit-card"></i>',
        'url' => dokan_get_navigation_url( 'settings/payment' ),
        'pos' => 250
    );

    if( isset( $nav['dashboard'] ) ) {
        unset( $nav['dashboard'] );
    }

    if( isset( $nav['withdraw'] ) ) {
        unset( $nav['withdraw'] );
    }

    return $nav;
}

add_filter( 'dokan_get_dashboard_nav', 'cc_dokan_get_dashboard_nav', 999 );

add_filter( 'dokan_seller_setup_wizard_url', 'setup_store_online' );

function setup_store_online( $url ) {
    $store_id   = get_current_user_id();
    $seller_settings = get_user_meta($store_id, 'dokan_profile_settings', true);
    $seller_settings['store_online'] = '1';
    update_user_meta($store_id, 'dokan_profile_settings', $seller_settings);
    return $url;
}
//SELL
add_action('wp_ajax_get_latlng_sellers', 'uf_get_latlng_sellers');
add_action('wp_ajax_nopriv_get_latlng_sellers', 'uf_get_latlng_sellers');

function uf_get_latlng_sellers() {
    $time = microtime();
    if( isset( $_POST['lat_array'] ) && isset( $_POST['lng_array'] ) ) {
        $lat_array = $_POST['lat_array'];
        sort( $lat_array, SORT_NUMERIC );
        $lng_array = $_POST['lng_array'];
        sort( $lng_array, SORT_NUMERIC );
        $new_latitude = isset( $_COOKIE['latitude'] ) ? $_COOKIE['latitude'] : null;
        $new_longitude = isset( $_COOKIE['longitude'] ) ? $_COOKIE['longitude'] : null;

        if ( (isset( $_POST['load_all'] ) && $_POST['load_all'] == 1) ) {

            // get all sellers
            //$sellers = dokan_get_sellers(array('number' => 100000));

            $sellers = dokan_get_sellers(array(
                'number' => 100000,
                'meta_query' => array(
                    array(
                        'key'     => 'dokan_enable_selling',
                        'value'   => 'yes',
                        'compare' => '='
                    ),
                    array(
                        'key'     => 'uf_latitude',
                        'value'   => $lat_array,
                        'compare' => 'BETWEEN',
                        'type' => 'DECIMAL(19,14)'
                    ),
                    array(
                        'key'     => 'uf_longitude',
                        'value'   => $lng_array,
                        'compare' => 'BETWEEN',
                        'type' => 'DECIMAL(19,14)'
                    ),
                    'relation' => 'AND'
                )
            ));
        } else {
            // get only stick  location sellers
            $sellers = dokan_get_sellers(array(
                'number' => 100000,
                'meta_query' => array(
                    array(
                        'key'     => 'dokan_enable_selling',
                        'value'   => 'yes',
                        'compare' => '='
                    ),
                    array(
                        'key'     => 'uf_latitude',
                        'value'   => $lat_array,
                        'compare' => 'BETWEEN',
                        'type' => 'DECIMAL(19,14)'
                    ),
                    array(
                        'key'     => 'uf_longitude',
                        'value'   => $lng_array,
                        'compare' => 'BETWEEN',
                        'type' => 'DECIMAL(19,14)'
                    ),
                    array(
                        'key'     => 'location_type',
                        'value'   => 'default',
                        'compare' => '!='
                    ),
                    'relation' => 'AND'
                )
            ));

        }

        $users = $sellers['users'];
        $map_locations = array();

        foreach( $users as $store_user ) {

            /*-------FEATURE: hide stores in not working hours-------*/
	        $dataa = get_user_meta($store_user->ID,'dokan_profile_settings', true);
	        if( isset($dataa['visible_only_in_shop_hours']) && $dataa['visible_only_in_shop_hours'] == 1 ) {//if we have this option enabled
                $store_status = check_if_store_open_now($store_user->ID);//returns only true/false (bool)
                if($store_status === false){
                    continue;
                }
            }
	        /*-------FEATURE: hide stores in not working hours-------*/


            // meta names:
            // uf_product_changed
            // uf_map_thumbnail
            if( get_user_meta($store_user->ID, 'uf_product_changed', true) == 'yes' ||
                empty(get_transient('uf_map_thumbnail' . $store_user->ID)) ) {
                $args = array(
                    'taxonomy'     => 'product_cat',
                    'hierarchical' => true,
                    'hide_empty'   => true
                );

                $all_categories = get_categories( $args );

                $count = -1;

                $is_choosen = 0;
                $choosen_cate = '';

                foreach ($all_categories as $cat) {

                    $query = new WP_Query( array(
                        'post_type' => 'product',
                        'product_cat' => $cat->slug,
                        'author' => $store_user->ID,
                    ) );

                    $products_count = $query->post_count;

                    if ( $products_count > 0 )
                    {
                        if ( $is_choosen == 1)
                        {
                            if ( $choosen_cate != 'services'  &&  $cat->slug == 'services' )
                            {
                                $top_cat_data = $cat;
                                $choosen_cate = $cat->slug;
                                $is_choosen = 1;
                            }
                        }else{
                            $top_cat_data = $cat;
                            $choosen_cate = $cat->slug;
                            $is_choosen = 1;
                        }

                    }

                }

                if( isset( $top_cat_data ) ) {

                    $thumbnail_id = get_term_meta($top_cat_data->term_id, 'thumbnail_id', true);
                    $temp = fly_get_attachment_image_src($thumbnail_id, array(32,32) );
                    $thumb_image = isset($temp['src']) ? $temp['src'] : '';

                }

                update_user_meta($store_user->ID, 'uf_product_changed', 'no');
                set_transient('uf_map_thumbnail' . $store_user->ID, isset($thumb_image) ? $thumb_image : '', 24 * 3600);

            } else {
                $thumb_image = get_transient('uf_map_thumbnail' . $store_user->ID);
            }

            //if( $user_data != -1 ) {



            $query = new WP_Query(array(
                'post_type' => 'product',
                'author' => $store_user->ID,
                'posts_per_page' => 50,
                'post_status' => 'publish'
                //add here select by opened time
            ));

            if ($query->post_count === 0) {
                continue;
            }

            ob_start();

            if ($query->have_posts()) {

                while ($query->have_posts()) : $query->the_post();

                    wc_get_template_part('content', 'product'); // template - product-details

                endwhile;

            } else {
                echo __('No products found');
            }

            wp_reset_postdata();

            $products = ob_get_clean();

            //}

            $location_type = get_user_meta($store_user->ID, 'stick_location', true);
            $location_type = empty( $location_type ) ? 'default' : $location_type;
            $map_locations[$store_user->ID] = array(
                'lat' => get_user_meta($store_user->ID, 'uf_latitude', true),
                'lng' => get_user_meta($store_user->ID, 'uf_longitude', true),
                'thumb' => !empty( $thumb_image ) ? $thumb_image : '',
                'seller_id' => $store_user->ID,
                'products' => $products,
                'stick_location' => $_POST['load_all'] != 1 || $location_type != 'default' ? 1 : 0
            );

        }

        wp_send_json_success( array(
            'location' => $map_locations,
            'currentlocation' => array(
                'lat' => $new_latitude,
                'lng' => $new_longitude,
                'from' => isset($_COOKIE["location_source"]) ? $_COOKIE["location_source"] : '?'
            )
        ) );

    } else {
        wp_send_json_error( __('Wrong coordinates' ) );
    }

}

function get_seller_location( $seller_id ) {

    $store_info = dokan_get_store_info( $seller_id );

    $map_location = isset ($store_info['location']) ? esc_attr($store_info['location']) : '';

    if( empty( $map_location ) && !empty( $store_info['address']['street_1'] ) ) {

        $address = array();

        if( isset( $store_info['address']['zip'] ) ) {

            $address[] = $store_info['address']['zip'];

        }

        if( isset( $store_info['address']['country'] ) ) {

            $address[] = $store_info['address']['country'];

        }

        if( isset( $store_info['address']['state'] ) ) {

            $address[] = $store_info['address']['state'];

        }

        if( isset( $store_info['address']['city'] ) ) {

            $address[] = $store_info['address']['city'];

        }

        if( isset( $store_info['address']['street_1'] ) ) {

            $address[] = $store_info['address']['street_1'];

        }

        if( isset( $store_info['address']['street_2'] ) ) {

            $address[] = $store_info['address']['street_2'];

        }

        $coordinates = get_latlng_from_address( implode(' ', $address) );

        $info = get_user_meta( $seller_id, 'dokan_profile_settings', true );

        $info['location'] = $map_location = isset( $coordinates['lat'] ) ? $coordinates['lat'] . ',' . $coordinates['lng'] : '';

        if (isset( $info['location'] )) {
            $location_arr = explode(',', $info['location']);
            if ( is_array($location_arr) && isset($location_arr[0]) && isset($location_arr[1]) ) {
                update_user_meta($seller_id, 'uf_latitude', $location_arr[0]);
                update_user_meta($seller_id, 'uf_longitude', $location_arr[1]);
            }
        }

        update_user_meta( $seller_id, 'dokan_profile_settings', $info );

    }

    return !empty( $map_location ) ? explode(",", $map_location) : array();

}

function get_latlng_from_address( $address ) {

    $api_key = dokan_get_option( 'gmap_api_key', 'dokan_general', false );

    $response = wp_remote_get('https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=' . $api_key );
    if( is_wp_error( $response ) ) return '';
    $gm_data = json_decode( $response['body'], true );

    if( isset( $gm_data['results'][0]['geometry']['location']['lat'] ) ) {
        return $gm_data['results'][0]['geometry']['location'];
    } else {
        return '';
    }

}

add_filter( 'msp_wc_slider_query_args', 'uf_msp_wc_slider_query_args', 99, 2 );

function uf_msp_wc_slider_query_args( $query, $data ) {
    if( isset( $_COOKIE['latitude'] ) && isset( $_COOKIE['longitude'] ) ) {

        $latitude = $_COOKIE['latitude'];
        $longitude = $_COOKIE['longitude'];

        //$sellers = dokan_get_sellers(array('number' => 100000));
        $sellers = dokan_get_sellers(array(
            'number' => 100000,
            'meta_query' => array(
                array(
                    'key'     => 'dokan_enable_selling',
                    'value'   => 'yes',
                    'compare' => '='
                ),
                array(
                    'key'     => 'uf_latitude',
                    'value'   => array((float)$latitude - 0.75, (float)$latitude + 0.75),
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL(19,14)'
                ),
                array(
                    'key'     => 'uf_longitude',
                    'value'   => array((float)$longitude - 1, (float)$longitude + 1),
                    'compare' => 'BETWEEN',
                    'type' => 'DECIMAL(19,14)'
                ),
                'relation' => 'AND'
            )
        ));

        $seller_ids = array();

        foreach( $sellers['users'] as $store_user ) {

            $seller_ids[] = $store_user->ID;
        }

        ksort($seller_ids, SORT_NUMERIC);

        $product_ids = array();
        foreach ($seller_ids as $seller) {

            $query_ids = new WP_Query( array(
                'post_type' => 'product',
                'author' => $seller,
                'posts_per_page' => -1,
                'fields' => 'ids'
            ) );
            $product_ids = array_merge($product_ids, $query_ids->get_posts());
        }

        $product_ids = array_unique( $product_ids );

        $query['post__in'] = $product_ids;
        $query['orderby'] = 'post__in';
    }

    return $query;

}

add_filter('masterslider_get_template_tag_value', 'uf_masterslider_get_template_tag_value', 99, 3);

function uf_masterslider_get_template_tag_value( $value, $tag_name, $product_post ) {

    global $post;
    switch( $tag_name ) {

        case 'shop_name':
            $product = wc_get_product( $product_post->ID );
            $store_info = dokan_get_store_info( $product_post->post_author );
            $value = $store_info['store_name'];
            break;

        case 'wc_price_html':

            $product = wc_get_product( $product_post->ID );

            $value = wc_price( wc_get_price_to_display( $product ) )."<br>".get_post_meta($product_post->ID, 'unit', true);

            break;
    }
    return $value;
}

add_filter('woocommerce_new_customer_data', 'newVendor');

function newVendor($array) {
    $array['role'] = 'seller';
    return $array;
}

add_filter('woocommerce_available_payment_gateways', 'cc_woocommerce_available_payment_gateways');

function cc_woocommerce_available_payment_gateways( $gateways ) {
    if( isset( $gateways['paypal'] ) && isset( WC()->cart ) && !WC()->cart->is_empty() ) {
        $cart = WC()->cart->get_cart();
        foreach( $cart as $cart_item ) {
            if( !isset( $cart_item['product_id'] ) ) continue;
            $post = get_post( $cart_item['product_id'] );
            $store_info = get_user_meta( $post->post_author, 'dokan_profile_settings', true);
            if( empty( $store_info['payment']['paypal']['email'] ) ||
                empty( $store_info['payment']['paypal']['enable'] ) ) {

                unset( $gateways['paypal'] );
                break;
            }

        }
    }
    return $gateways;
}

add_action( 'init', 'custom_remove_footer_credit', 10 );

function custom_remove_footer_credit () {
    remove_action( 'storefront_header', 'storefront_product_search', 40 );
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
    //remove_action( 'storefront_footer' );
}

add_filter("wpcf7_mail_components", "change_recipient", 99, 3);

function change_recipient( $components, $answers, $mail ) {

    $product_and_author = $_POST["subject"];

    $product_and_author = base64_decode($product_and_author);

    $product_and_author = json_decode($product_and_author, true);

    $seller_email = get_userdata( $product_and_author['seller_id'] );

    $components['recipient'] = $seller_email->get('user_email');

    $components['subject'] .= ' "'.$product_and_author['product_name'].'"';

    return $components;

}

function check_payment_availability ($seller_id) {

    $store_info = dokan_get_store_info( $seller_id );

    $payment_methods = dokan_withdraw_get_active_methods();


    // get payment methods from $payment_methods and compare with enabled in $store_info

    if (!empty($payment_methods)) {

        $avalaible_methods = array();

        foreach ($payment_methods as $method) {

            if ( isset( $store_info['payment'][$method]['enable'] ) && (bool)$store_info['payment'][$method]['enable']) {

                $avalaible_methods[] = $method;

            }
        }

    }

    if (!empty($avalaible_methods)) {

        return true;

    } else return false;

}

add_action('dokan_new_product_added', 'add_product_units');
add_action('dokan_product_updated', 'edit_product_units');

function add_product_units($product_id) {

    error_log('add_product_units');

    parse_str($_POST["postdata"], $output);

    $units = $output["_units"];

    if ($units === 'custom') {

        $custom_units = $output["custom_units"];

        update_post_meta($product_id, 'unit', $custom_units);

    } else {

        update_post_meta($product_id, 'unit', $units);

    }

}

function edit_product_units($product_id) {

    $units = $_POST["_units"];

    if ($units === 'custom') {

        $custom_units = $_POST["custom_units"];

        update_post_meta($product_id, 'unit', $custom_units);

    } else {

        update_post_meta($product_id, 'unit', $units);

    }

}

add_filter('option_woocommerce_paypal_settings', 'uf_option_woocommerce_paypal_settings');
function uf_option_woocommerce_paypal_settings( $option ) {
    $uf_store_info = array();
    if ( defined( 'WOOCOMMERCE_CHECKOUT' ) ) {
        if ( !WC()->cart->is_empty() ) {
            $array = array_values(WC()->cart->get_cart());
            $item = array_shift($array);

            $product_obj = get_post( $item['product_id'], ARRAY_A );
            $store_info = dokan_get_store_info( $product_obj['post_author'] );
            $uf_store_info = $store_info;
        }
    } else {
        if ( empty( $_REQUEST['txn_id'] ) || !empty( $uf_store_info ) ) {
            return $option;
        }
        $order_id    = uf_get_paypal_order( wc_clean( stripslashes( $_REQUEST['custom'] ) ) );
        $order = wc_get_order( $order_id );

        $items = array_values($order->get_items());

        $product = $items[0];

        $product_props = $product->get_data();

        $product_obj = get_post( $product_props['product_id'], ARRAY_A );
        $store_info = dokan_get_store_info( $product_obj['post_author'] );

        $uf_store_info = $store_info;
    }
    if (empty($uf_store_info)) {
        return $option;
    }
    $method = $uf_store_info['payment']['paypal'];

    /*$option['api_username'] = $method['user'];

    $option['api_password'] = $method['pwd'];

    $option['api_signature'] = $method['signature'];*/

    $option['email'] = $method['email'];
    /*$option['receiver_email'] = $method['email'];*/
    return $option;

}

function uf_get_paypal_order( $raw_custom ) {
    // We have the data in the correct format, so get the order.
    if ( ( $custom = json_decode( $raw_custom ) ) && is_object( $custom ) ) {
        $order_id  = $custom->order_id;
        $order_key = $custom->order_key;

        // Fallback to serialized data if safe. This is @deprecated in 2.3.11
    } elseif ( preg_match( '/^a:2:{/', $raw_custom ) && ! preg_match( '/[CO]:\+?[0-9]+:"/', $raw_custom ) && ( $custom = maybe_unserialize( $raw_custom ) ) ) {
        $order_id  = $custom[0];
        $order_key = $custom[1];

        // Nothing was found.
    } else {
        WC_Gateway_Paypal::log( 'Order ID and key were not found in "custom".', 'error' );
        return false;
    }

    if ( ! $order = wc_get_order( $order_id ) ) {
        // We have an invalid $order_id, probably because invoice_prefix has changed.
        $order_id = wc_get_order_id_by_order_key( $order_key );
        $order    = wc_get_order( $order_id );
    }

    if ( ! $order || $order->get_order_key() !== $order_key ) {
        WC_Gateway_Paypal::log( 'Order Keys do not match.', 'error' );
        return false;
    }

    return $order_id;
}

add_action('woocommerce_add_to_cart', 'check_seller_wc_add_to_cart', 10, 6);

function check_seller_wc_add_to_cart ( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {

    if ( !WC()->cart->is_empty() ) {

        foreach( WC()->cart->get_cart() as $key=>$value ) {

            if( $key == $cart_item_key ) continue;

            $item = $value;

            break;
        }

        if( isset( $item ) ) {

            $cart_product_obj = get_post( $item['product_id'], ARRAY_A );

            $current_product_obj = get_post( $product_id, ARRAY_A );

            if ($cart_product_obj['post_author'] !== $current_product_obj['post_author']) {

                //WC()->cart->remove_cart_item( $cart_item_key );
                //throw new Exception( __( 'You cannot add that product to the cart because you already have some products from the other merchant in your cart. If you need to buy a product from a different merchant, please complete your current order and then create a new one.', 'woocommerce' ) );

                WC()->cart->empty_cart();
                WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data );

            }
        }
    }
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_first_name']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['billing']['billing_phone']);
    //unset($fields['order']['order_comments']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_email']);
    unset($fields['billing']['billing_city']);
    return $fields;
}

function uf_change_post_status () {

    $ufPostId = $_POST['ufPostId'];

    $current_post = get_post( $ufPostId, 'ARRAY_A' );

    if ( $current_post['post_status'] == 'publish' ) {

        $current_post['post_status'] = 'draft';

    } else {

        $current_post['post_status'] = 'publish';

    }

    wp_update_post( $current_post );

    exit;

}

add_action('wp_ajax_change_post_status', 'uf_change_post_status');

add_action('wp_ajax_nopriv_change_post_status', 'uf_change_post_status');

function uf_change_product_price () {

    $ufProductId = $_POST['ufProductId'];

    $ufNewProductPrice = $_POST['ufNewProductPrice'];
    $product = wc_get_product( $ufProductId );
    $product->set_regular_price( $ufNewProductPrice );
    $product->save();

    echo $ufProductId;

    exit;

}

add_action('wp_ajax_change_product_price', 'uf_change_product_price');

add_action('wp_ajax_nopriv_change_product_price', 'uf_change_product_price');

// Force a user to be a seller (facebook registration fix)
add_filter( 'ywsl_new_user_role', 'change_customer_to_vendor' );

function change_customer_to_vendor($role) {
    return 'seller';
}

add_filter('dokan_get_post_status', 'uf_dokan_get_post_status');
function uf_dokan_get_post_status( $array ){
    $array['draft'] = __('Offline');
    return $array;
}

remove_action( 'storefront_header', 'storefront_site_branding' );

function storefront_site_branding() {
    ?>
    <div class="site-branding">
        <?php uf_storefront_site_title_or_logo(); ?>
    </div>
    <?php
}

add_filter('get_custom_logo', 'uf_get_custom_logo');
function uf_get_custom_logo( $html ) {
    $html = preg_replace('/href="([^"]*)"/im', 'href="javascript: void(0);"', $html );
    return $html;
}

function uf_storefront_site_title_or_logo( $echo = true ) {
    if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
        $logo = get_custom_logo();
        $html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;
    } elseif ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
        // Copied from jetpack_the_site_logo() function.
        $logo    = site_logo()->logo;
        $logo_id = get_theme_mod( 'custom_logo' ); // Check for WP 4.5 Site Logo
        $logo_id = $logo_id ? $logo_id : $logo['id']; // Use WP Core logo if present, otherwise use Jetpack's.
        $size    = site_logo()->theme_size();
        $html    = sprintf( '<a href="%1$s" class="site-logo-link" rel="home" itemprop="url">%2$s</a>',
            'javascript: void(0);',
            wp_get_attachment_image(
                $logo_id,
                $size,
                false,
                array(
                    'class'     => 'site-logo attachment-' . $size,
                    'data-size' => $size,
                    'itemprop'  => 'logo'
                )
            )
        );

        $html = apply_filters( 'jetpack_the_site_logo', $html, $logo, $size );
    } else {
        $tag = is_home() ? 'h1' : 'div';

        $html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a>';

        if ( is_user_logged_in() ) {
            $current_user = wp_get_current_user();
            $store_info = dokan_get_store_info( $current_user->ID );
            $store_name = $store_info["store_name"];
            if ( strlen($store_name) > 7 ) {
                $store_short_name = substr($store_name, 0, 6) . '...';
                $html .=  ' <a class="uf-store-shortname" href="//www.urbanfresh.org/myshop/products/">' . $store_short_name . '</a>';
            } else {
                $html .=  ' <a class="uf-store-shortname" href="//www.urbanfresh.org/myshop/products/">' . $store_name . '</a>';
            }
        }

        $html .= '</' . esc_attr( $tag ) . '>';

        if ( '' !== get_bloginfo( 'description' ) ) {
            $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
        }

    }

    if ( ! $echo ) {
        return $html;
    }

    echo $html;
}


function uf_wc_product_images ( $product_id, $data) {
    error_log('uf_wc_product_images');
    if (isset($data['uf_image_gallery_ids'])) {
        $image_ids = explode(',', $data['uf_image_gallery_ids']);
        $product = wc_get_product($product_id);
        $product->set_image_id($image_ids[0]);
        array_shift($image_ids);
        $product->set_gallery_image_ids($image_ids);
        $product->save();
    }
}

function uf_wc_product_images_post ( $product_id) {
    uf_wc_product_images($product_id, $_POST);
}

add_action( 'dokan_new_product_added', 'uf_wc_product_images', 10, 2);
add_action( 'dokan_product_updated', 'uf_wc_product_images_post', 10, 2);

function uf_ajax_crop_handler () {

    if( !empty($_POST["single_image_id"]) && !empty($_POST["cropbox_width"]) &&
        isset($_POST["x"]) && isset($_POST["y"]) &&
        !empty($_POST["w"]) && !empty($_POST["h"]) ) {

        $src = get_attached_file( (int)$_POST["single_image_id"] );
        $image = wp_get_image_editor( $src );
        //var_dump($src, $image); exit;
        if( !is_wp_error( $image ) ) {
            $image->set_quality( 100 );
            $image_size = $image->get_size();
            $factor = $image_size['width']/(int)$_POST['cropbox_width'];

            $image->crop( (int)($_POST['x']*$factor), (int)($_POST['y']*$factor), (int)($_POST['w']*$factor), (int)($_POST['h']*$factor) );

            $output_filename = $image->generate_filename('crop');
            $result = $image->save( $output_filename );
            if( !is_wp_error( $result ) ) {
                uf_image_delete_attachment( (int)$_POST["single_image_id"] );
                if (!file_exists(dirname($src)))
                    mkdir(dirname($src), 0777, true);
                //rename($output_filename, $src);

                $attachment_metadata = wp_generate_attachment_metadata( (int)$_POST["single_image_id"], $output_filename );
                wp_update_attachment_metadata( (int)$_POST["single_image_id"], $attachment_metadata );
                update_attached_file( (int)$_POST["single_image_id"], $output_filename);

                $response_src = wp_get_attachment_url( (int)$_POST["single_image_id"] );
                wp_send_json_success( $response_src );
            }
        }

    } else {
        wp_send_json_error( __("Can't crop. Something wrong with crop parameters" ) );
    }
}

add_action('wp_ajax_uf_ajax_crop_handler', 'uf_ajax_crop_handler');
add_action('wp_ajax_nopriv_uf_ajax_crop_handler', 'uf_ajax_crop_handler');

function uf_image_delete_attachment( $post_id ) {
    $meta = wp_get_attachment_metadata( $post_id );
    $backup_sizes = get_post_meta( $post_id, '_wp_attachment_backup_sizes', true );
    $file = get_attached_file( $post_id );

    if ( is_multisite() )
        delete_transient( 'dirsize_cache' );

    $uploadpath = wp_get_upload_dir();

    if ( ! empty($meta['thumb']) ) {
        $thumbfile = str_replace(basename($file), $meta['thumb'], $file);
        /** This filter is documented in wp-includes/functions.php */
        $thumbfile = apply_filters( 'wp_delete_file', $thumbfile );
        @ unlink( path_join($uploadpath['basedir'], $thumbfile) );
    }

    // Remove intermediate and backup images if there are any.
    if ( isset( $meta['sizes'] ) && is_array( $meta['sizes'] ) ) {
        foreach ( $meta['sizes'] as $size => $sizeinfo ) {
            $intermediate_file = str_replace( basename( $file ), $sizeinfo['file'], $file );
            /** This filter is documented in wp-includes/functions.php */
            $intermediate_file = apply_filters( 'wp_delete_file', $intermediate_file );
            @ unlink( path_join( $uploadpath['basedir'], $intermediate_file ) );
        }
    }

    if ( is_array($backup_sizes) ) {
        foreach ( $backup_sizes as $size ) {
            $del_file = path_join( dirname($meta['file']), $size['file'] );
            /** This filter is documented in wp-includes/functions.php */
            $del_file = apply_filters( 'wp_delete_file', $del_file );
            @ unlink( path_join($uploadpath['basedir'], $del_file) );
        }
    }
    wp_delete_file( $file );
}

// Change "Read More" button into "View Details"
add_filter( 'woocommerce_product_add_to_cart_text', 'uf_custom_woo_button_text', 10, 2 );

function uf_custom_woo_button_text( $text, $product ) {
    return $product->is_purchasable() && $product->is_in_stock() ? __( 'Add to cart', 'woocommerce' ) : __( 'View Details', 'woocommerce' );
}

// beforeunload preloader if !checkout
function uf_preloader() {
    wp_enqueue_script('uf_preloader', get_stylesheet_directory_uri() . '/js/preloader.js', array('jquery'), 1.0);
}
add_action( 'wp_enqueue_scripts', 'uf_preloader', 1 );

function woocommerce_template_loop_add_to_cart2( $args = array() ) {
    global $product;

    if ( $product ) {
        $defaults = array(
            'quantity' => 1,
            'class'    => implode( ' ', array_filter( array(
                'button',
                'product_type_' . $product->get_type(),
                '',
                $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
            ) ) ),
        );

        $args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

        wc_get_template( 'loop/add-to-cart2.php', $args );
    }
}

add_action( 'woocommerce_after_shop_loop_item2', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_after_shop_loop_item2', 'woocommerce_template_loop_add_to_cart2', 10 );

function uf_dokan_setup_introduction() {

    global $dokan_obj;

    $current_user_id = get_current_user_id();
    $store_info = dokan_get_store_info( $current_user_id );
    if (empty($store_info['location'])) {
        $store_info['location'] = isset($_COOKIE['latitude']) ? $_COOKIE['latitude'] . ',' . $_COOKIE['longitude'] : '';
    }
    if ( empty($store_info['address']) ) {
        $uf_location = isset($_COOKIE['uf_location']) ? maybe_unserialize($_COOKIE['uf_location']) : array();
        $store_info['address']['zip'] = isset($uf_location['zip']) ? $uf_location['zip'] : '';
        $store_info['address']['city'] = isset($uf_location['city']) ? $uf_location['city'] : '';
        $store_info['address']['country'] = isset($uf_location['country_code']) ? $uf_location['country_code'] : '';
        $store_info['address']['state'] = isset($uf_location['state_code']) ? $uf_location['state_code'] : '';
    }

    update_user_meta( $current_user_id, 'dokan_profile_settings', $store_info );

    if (!empty( $store_info['location'] )) {
        $location_arr = explode(',', $store_info['location']);
        if ( is_array($location_arr) && isset($location_arr[0]) && isset($location_arr[1]) ) {
            update_user_meta($current_user_id, 'uf_latitude', $location_arr[0]);
            update_user_meta($current_user_id, 'uf_longitude', $location_arr[1]);
        }
    }

    $dashboard_url = dokan_get_navigation_url();
    ?>
    <h1><?php _e( 'Welcome to Urban Fresh!', 'dokan-lite' ); ?></h1>
    <p><?php _e( 'Use this setup wizard to start selling quickly or click Skip to go directly to your Seller Dashboard.', 'dokan-lite' ); ?></p>
    <p><!--<?php _e( 'No time right now? If you don’t want to go through the wizard, you can skip and return to the Store!', 'dokan-lite' ); ?>--></p>
    <p class="wc-setup-actions step">
        <a href="<?php echo esc_url( $dokan_obj->get_next_step_link() ); ?>" class="button-primary button button-large button-next"><?php _e( 'Start Wizard', 'dokan-lite' ); ?></a>
        <a href="<?php echo esc_url( $dashboard_url ); ?>" class="button button-large"><?php _e( 'Skip', 'dokan-lite' ); ?></a>
    </p>
    <?php
}

// Related products
add_action( 'woocommerce_after_single_product_summary', 'uf_related_products_by_seller', 25 );

function uf_related_products_by_seller () {
    global $product;

    if ( ! $product ) {
        return;
    }

    $query = new WP_Query( array(
        'post_type' => 'product',
        'author' => $product->__get('post')->post_author,
        'posts_per_page' => 50,
        'post__not_in'   => array(get_the_ID())
    ) );

    wc_get_template( 'single-product/seller-product-list.php', array(
        'query' => $query
    ));
}

// Turn "Add to cart" button into "View Details"
// add_filter('woocommerce_loop_add_to_cart_link', 'uf_wc_loop_add_to_cart_link', 9, 2);

function uf_wc_loop_add_to_cart_link ($link, $product) {
    if ( isset( $_POST['action'] ) && $_POST['action'] != 'get_latlng_sellers' && wp_doing_ajax() ) {
        return $link;
    }
    return sprintf( '<a rel="nofollow" href="%s" class="%s">%s</a>',
        esc_url( get_permalink( $product->get_id() ) ),
        esc_attr( 'button' ),
        esc_html( 'View Details' )
    );
}

//Disable double google maps script enabling by geo-my-wp
add_filter('gmw_google_maps_api', 'uf_gmw_google_maps_api');
function uf_gmw_google_maps_api () {
    return false;
}

add_shortcode( 'location_debug', 'uf_location_debug_shortcode' );

function uf_location_debug_shortcode () {
    $current_lat = isset($_COOKIE["latitude"]) ? $_COOKIE["latitude"] : ' ';
    $current_lng = isset($_COOKIE["longitude"]) ? $_COOKIE["longitude"] : ' ';
    $cookie_from = isset($_COOKIE["location_source"]) ? $_COOKIE["location_source"] : '?';
    echo '<div id="uf-debug" style="clear: both; position: relative; top: 355px;">Lat: ' . $current_lat . ' Lng: ' . $current_lng . ' ' . $cookie_from . '</div>';
}

// To check whether product has been edited ao added
add_action( 'dokan_new_product_added', 'uf_sync_on_product_save' );
add_action( 'dokan_product_updated', 'uf_sync_on_product_save' );

function uf_sync_on_product_save( ) {
    error_log('uf_sync_on_product_save');
    update_user_meta(get_current_user_id(), 'uf_product_changed', 'yes');
}

// prevent woogeolocation and dokan from updating
add_filter('site_transient_update_plugins', 'remove_update_notification');
function remove_update_notification($value) {
    if( isset( $value->response[ "dokan-lite/dokan.php" ] ) ) {
        unset($value->response[ "dokan-lite/dokan.php" ]);
    }
    if( isset( $value->response[ "woogeolocation/woogeolocation.php" ] ) ) {
        unset($value->response[ "woogeolocation/woogeolocation.php" ]);
    }
    return $value;
}

add_action('template_redirect', 'uf_template_redirect');
function uf_template_redirect() {
    global $wp;
    if (isset( $wp->query_vars['customer-logout'] ) && ('true' === $wp->query_vars['customer-logout'] ) ) {
        wp_clear_auth_cookie();
        wp_redirect( esc_url_raw( get_home_url() . '/map' ) );
        exit;
    }

    if( dokan_is_seller_dashboard() ) {
        global $uf_seller_id;
        $uf_seller_id = get_current_user_id();
    }
}

function custom_script() {
    global $post;

    wp_enqueue_script('jquery');
    wp_enqueue_script('wp-util');

    wp_register_script('uf_fullscreen_popup', get_stylesheet_directory_uri() . '/assets/js/jquery.fullscreen-popup.min.js', array( 'jquery' ), 1.0, true );

    wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/script.js', array( 'jquery', 'wp-util', 'uf_fullscreen_popup' ), 1.0, true );

    if( is_product() ) {
	    wp_enqueue_script( 'custom_script', get_stylesheet_directory_uri() . '/js/custom_script.js', array( 'jquery' ), 1.0, true );
    }

    if( $post->ID == MAP_PAGE_ID || $post->ID == SLIDER_PAGE_ID ) {
        wp_enqueue_script('jquery.ui.touch-punch', get_stylesheet_directory_uri() . '/js/jquery.ui.touch-punch.min.js', array('jquery', 'jquery-ui-draggable'));
	    wp_enqueue_script( 'switcher_script', get_stylesheet_directory_uri() . '/js/switcher_script.js', array( 'jquery.ui.touch-punch' ) );
    }



}
add_action( 'wp_enqueue_scripts', 'custom_script' );

function remove_scripts() {
    global $post, $wp_filter;

    if( isset( $wp_filter['wp_footer']->callbacks[10] ) ) {
        $wp_filter['wp_footer']->callbacks[10] = array_filter( $wp_filter['wp_footer']->callbacks[10], function($val) {
            if( isset( $val['function'][1] ) && $val['function'][1] == 'cl_template' ) {
                return false;
            } else {
                return true;
            }
        } );
    }

    if( !is_product() ) {
        wp_deregister_script( 'jquery-ui-spinner' );
        wp_deregister_style( 'jquery-ui-smoothness' );
        wp_deregister_script( 'google-recaptcha' );
    }

    if( function_exists('dokan_get_option') ) {
	    $page_id = dokan_get_option( 'dashboard', 'dokan_pages' );
	    $dokan_params = array(
            'products',
            'new-product',
            'orders',
            'withdraw',
            'settings',
            'edit-account',
            'edit'
        );
	    $flag = isset( $_GET['page'] ) && $_GET['page'] == 'dokan-seller-setup';
	    foreach( $dokan_params as $p ) {
	        if( get_query_var( $p ) ) {
	            $flag = true;
	            break;
            }
        }

	    if( $post->ID != $page_id && !$flag ) {
		    //dokan
		    wp_deregister_style( 'dokan-fontawesome' );
		    wp_deregister_style( 'dashicons' );
		    wp_deregister_style( 'dokan-style' );
		    wp_deregister_style( 'dokan-fontawesome' );
	        wp_deregister_script( 'jquery-ui-datepicker' );
	        wp_deregister_script( 'dokan-script' );
	    }
    }
    /*global $wp_query;
    var_dump($wp_query);*/
    if( $post->ID != MAP_PAGE_ID && $post->ID != SLIDER_PAGE_ID ) {
        wp_deregister_script( 'jquery-ui-draggable');
    }

    if( $post->ID != MAP_PAGE_ID && $post->ID != SLIDER_PAGE_ID && $post->ID != $page_id && !$flag ) {
        wp_deregister_script( 'jquery-ui');
        wp_deregister_script( 'jquery-ui-mouse');
        wp_deregister_script( 'jquery-ui-widget');
    }

    //geo-my-wp
    wp_deregister_style( 'gmw-style' );
    wp_deregister_style( 'gmw-cl-style' );
    wp_deregister_style( 'font-awesome' );
    wp_deregister_style( 'gmw-cl-style-dep' );
    wp_deregister_style( 'comment-reply' );

    //quantities-and-units-for-woocommerce
    wp_deregister_style( 'wcqu_quantity_styles' );

    //yith-woocommerce-social-login
    wp_deregister_style( 'ywsl_frontend' );
    wp_deregister_script( 'ywsl_frontend' );

    if( $post->ID != SLIDER_PAGE_ID ) {
	    //masterslider
	    wp_deregister_style( 'masterslider-main' );
	    wp_deregister_style( 'ms-main' );
	    wp_deregister_script( 'masterslider-core' );
    }

    //woogeolocation
    wp_deregister_script( 'geocomplete.min' );
    wp_deregister_style('woogeolocation_map_css' );

    if( !is_checkout() ) {
	    wp_deregister_script( 'woocommerce' );
        wp_deregister_script( 'js-cookie' );
        //woocommerce
        wp_deregister_script( 'jquery-blockui' );
    }
    wp_deregister_script( 'wc-cart-fragments' );

    //common js
    wp_deregister_script( 'wp-embed');
    wp_deregister_script( 'storefront-header-cart');
    wp_deregister_script( 'wc-password-strength-meter');
    wp_deregister_script( 'password-strength-meter');
    wp_deregister_script( 'zxcvbn-async');

    /*if( !is_page( wc_get_page_id( 'myaccount' ) ) || is_user_logged_in() ) {
        wp_deregister_script('hm_wcps');
    }*/

    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10 );
}
add_action( 'wp_print_scripts', 'remove_scripts', 1 );
add_action( 'wp_enqueue_scripts', 'remove_scripts', PHP_INT_MAX );
add_action( 'wp_footer', 'remove_scripts', 9 );

// remove some required fields from dokan registration
add_filter('dokan_seller_registration_required_fields', 'uf_dokan_seller_registration_required_fields');
function uf_dokan_seller_registration_required_fields ($fields) {
    if (isset($fields['fname'])) unset($fields['fname']);
    if (isset($fields['lname'])) unset($fields['lname']);
    if (isset($fields['phone'])) unset($fields['phone']);
    return $fields;
}

// To delete attachments on product deletion
add_action('before_delete_post','uf_delete_product_images', 10, 1);

function uf_delete_product_images($post_id) {
    $product = wc_get_product($post_id);

    if ( !$product ) {
        return;
    }

    $featured_image_id = $product->get_image_id();
    $image_galleries_id = $product->get_gallery_image_ids();

    if(!empty($featured_image_id)) {
        wp_delete_post($featured_image_id);
    }

    if(!empty($image_galleries_id)) {
        foreach($image_galleries_id as $single_image_id) {
            wp_delete_post($single_image_id);
        }
    }
}

function my_woocommerce_add_error( $error ) {
    return str_replace('An account is already registered with your email address. Please log in.','An account is already registered with your email address. <a href="https://www.urbanfresh.org/shop-account/?exists#showlogin">Please log in</a>.', $error);
}

add_filter( 'woocommerce_add_error', 'my_woocommerce_add_error' );

function uf_check_products_availability( $args, $content ) {
    if( isset( $_COOKIE['latitude'] ) && isset( $_COOKIE['longitude'] ) ) {
        $latitude = $_COOKIE['latitude'];
        $longitude = $_COOKIE['longitude'];
    } else {
        $array = get_coordinates_by_ip();
        $latitude = $array['lat'];
        $longitude = $array['lng'];
    }

    $latitude_arr = array((float)$latitude - 0.75, (float)$latitude + 0.75);
    $longitude_arr = array((float)$longitude - 1, (float)$longitude + 1);
    $sellers = dokan_get_sellers(array(
        'number' => 100000,
        'meta_query' => array(
            array(
                'key'     => 'dokan_enable_selling',
                'value'   => 'yes',
                'compare' => '='
            ),
            array(
                'key'     => 'uf_latitude',
                'value'   => $latitude_arr,
                'compare' => 'BETWEEN',
                'type' => 'DECIMAL(19,14)'
            ),
            array(
                'key'     => 'uf_longitude',
                'value'   => $longitude_arr,
                'compare' => 'BETWEEN',
                'type' => 'DECIMAL(19,14)'
            ),
            'relation' => 'AND'
        )
    ));

    $seller_ids = $product_ids = array();
    foreach( $sellers['users'] as $store_user ) {
        $seller_ids[] = $store_user->ID;
    }

    foreach ($seller_ids as $seller) {
        $query_ids = new WP_Query( array(
            'post_type' => 'product',
            'author' => $seller,
            'posts_per_page' => -1,
            'fields' => 'ids'
        ) );
        $product_ids = array_merge($product_ids, $query_ids->get_posts());
    }

    if ( count( $product_ids ) > 0 ) {
        return do_shortcode( $content );
    } else {
        if ( is_user_logged_in() ) {
            $href = '//www.urbanfresh.org/myshop/products/?add_product=1';
        } else {
            $href = '//www.urbanfresh.org/shop-account/#showregister';
        }
        return '<div class="uf-no-sellers-content">
                    <div class="">
                        <h1 style="text-align: center;">No sellers in your area</h1>
                        <div style="text-align: center;" class="wc-setup-next-steps">
                            <div class="wc-setup-next-steps-first final-button">
                                <a class="button button-primary button-large" href="'. $href .'">Create Shop Today</a>
                            </div>
                        </div>
                    </div>
                    </div>';
    }
}
add_shortcode('check_products_availability', 'uf_check_products_availability');

function uf_init() {
    //global $post;
    /*if( !empty( $_POST['dokan_update_stick_location'] ) ) {
        $stick_location = !empty( $_POST['stick_location'] ) ? $_POST['stick_location'] : 'no';
        $store_info = dokan_get_store_info( get_current_user_id() );
        $store_info['stick_location'] = $stick_location;
        update_user_meta( get_current_user_id(), 'dokan_profile_settings', $store_info );
    }*/

    if( is_user_logged_in() ) {
	    update_user_meta( get_current_user_id(), 'uf_last_vendor_activity', time() );
    }

    if( !wp_next_scheduled( 'uf_check_seller_activity' ) ) {
        wp_schedule_event( time(), 'daily', 'uf_check_seller_activity' );
    }
}
add_action('init', 'uf_init');

add_action( 'uf_check_seller_activity', 'uf_check_seller_activity' );

function uf_check_seller_activity() {
    $sellers = dokan_get_sellers(array(
        'number' => 100000,
        'meta_query' => array(
            array(
                'key'     => 'dokan_enable_selling',
                'value'   => 'yes',
                'compare' => '='
            ),
            array(
                'key'     => 'uf_last_vendor_activity',
                'value'   => strtotime("-2 month"),
                'compare' => '<='
            ),
            'relation' => 'AND'
        )
    ));
    $users = $sellers['users'];
    foreach ($users as $store_user) {
        $store_info = dokan_get_store_info( $store_user->ID );
        $store_info['store_online'] = '0';
        update_user_meta( $store_user->ID, 'dokan_profile_settings', $store_info );
    }
}

function cc_after_login_redirect( $redirect_to, $user ) {
    if ( user_can( $user, 'dokandar' ) ) {
        $seller_dashboard = dokan_get_option( 'dashboard', 'dokan_pages' );

        if ( $seller_dashboard != -1 ) {
            $redirect_to = get_permalink( $seller_dashboard ) . 'products/';
        }
    }

    return $redirect_to;
}

add_filter( 'woocommerce_login_redirect', 'cc_after_login_redirect' , 99, 2 );

add_action('wp_ajax_uf_send_contact_form', 'uf_send_contact_form');
add_action('wp_ajax_nopriv_uf_send_contact_form', 'uf_send_contact_form');
function uf_send_contact_form() {
    if( !empty( $_POST['user_id'] ) ) {
        $user_id = $_POST['user_id'];
    } else {
        $user_id = 0;
    }
    if( !empty( $_POST['product_id'] ) ) {
        $product_id = $_POST['product_id'];
    } else {
        $product_id = 0;
    }
    if( !empty( $_POST['name'] ) ) {
        $name = $_POST['name'];
    } else {
        wp_send_json_error();
    }
    if( !empty( $_POST['email'] ) ) {
        $email = $_POST['email'];
    } else {
        wp_send_json_error();
    }

    $message = isset( $_POST['msg'] ) ? $_POST['msg'] : '';

    if( $user_id ) {
	    $user = get_userdata( $user_id );
    }
    if( $product_id ) {
	    $product = wc_get_product( $product_id );
    }

    $subject = isset($product) ? 'New UrbanFresh Inquiry for ' . $product->get_name() : 'New feedback on UrbanFresh';
    $body = "<p>From: $name <$email></p>

<p>Message Body:</p>
<p>$message</p>

<p>--</p>
<p>This e-mail was sent from a contact form on Urban Fresh (http://www.urbanfresh.org)</p>";

    $headers = array();
    $headers[] = 'From: ' . $name . ' <wordpress@urbanfresh.org>';
    if( isset( $user ) ) {
	    $headers[] = 'Bcc: Admin <wordpress@urbanfresh.org>';
    }
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    $email = isset( $user ) ? $user->user_email : 'feedback@urbanfresh.org';
    $result = wp_mail( $email, $subject, $body, $headers );
    if( $result ) {
        wp_send_json_success();
    } else {
        wp_send_json_error('wp-mail');
    }
}

function uf_get_days_of_week() {
    return array(
        'Su' => 'Sunday',
        'Mo' => 'Monday',
        'Tu' => 'Tuesday',
        'We' => 'Wednesday',
        'Th' => 'Thursday',
        'Fr' => 'Friday',
        'Sa' => 'Saterday'
    );
}

function uf_get_shop_days( $days ) {
    if( empty( $days ) ) return '';

    if( count( $days ) == 7 ) {
        return 'Daily';
    }

    $default = array_keys( uf_get_days_of_week() );
    $start = array_search( $days[0], $default );
    $end = array_search( $days[ count( $days ) - 1 ], $default );
    if( $end - $start + 1 == count( $days ) ) {
        return $days[0] . ' - ' . $days[ count( $days ) - 1 ];
    }

    return implode(', ', $days );
}

function get_coordinates_by_ip( $ip = '' ) {
    global $ip_coordinates_cache;
    if( !empty( $ip_coordinates_cache ) ) return $ip_coordinates_cache;
    $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];
    $response = wp_remote_get( 'http://freegeoip.net/json/' . $ip );
    if( !is_wp_error( $response ) ) {
        $body = wp_remote_retrieve_body( $response );
        $array = json_decode( $body, true );
        if( isset( $array['latitude'] ) && isset( $array['longitude'] ) ) {
            $ip_coordinates_cache[ $ip ] = array(
                'lat' => $array['latitude'],
                'lng' => $array['longitude'],
                'city' => isset( $array['city'] ) ? $array['city'] : ''
            );
            return $ip_coordinates_cache[ $ip ];
        }
    }
    return false;
}

add_action('wp_head', 'page_in_iframe');

function page_in_iframe(){
    $reload_link = $_SERVER['REDIRECT_REDIRECT_SCRIPT_URI'];

    echo '
        <script>
            jQuery(document).ready(function() {
                var width = jQuery( document ).width();
                if(width > 767){
                    jQuery("body").empty();
                    jQuery("<iframe>", {
                        src: window.location.href,
                        id:  "myFrame",
                        frameborder: 0,
                        align: "center",
                        scrolling: "yes"
                   }).appendTo("body");
                    jQuery("iframe#myFrame").css("display","block");
                    jQuery("iframe#myFrame").css("margin","0 auto");
                    jQuery("iframe#myFrame").width(766);
                    jQuery("iframe#myFrame").height("100%");
                }
            });
        </script>
    ';
}
