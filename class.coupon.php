<?php
namespace UF;

if( !defined('ABSPATH') ) exit;

if( !class_exists('UF\Coupon') ) {
	class Coupon {
		function __construct() {
			add_filter('woocommerce_coupon_is_valid', array( &$this, 'woocommerce_coupon_is_valid' ), 999, 2);
			add_action( 'woocommerce_order_status_pending', array( &$this, 'wc_update_coupon_usage_counts' ) );
			add_action( 'woocommerce_order_status_completed', array( &$this, 'wc_update_coupon_usage_counts' ) );
			add_action( 'woocommerce_order_status_processing', array( &$this, 'wc_update_coupon_usage_counts' ) );
			add_action( 'woocommerce_order_status_on-hold', array( &$this, 'wc_update_coupon_usage_counts' ) );
			add_action( 'woocommerce_order_status_cancelled', array( &$this, 'wc_update_coupon_usage_counts' ) );
		}

		function wc_update_coupon_usage_counts( $order_id ) {
			if ( ! $order = wc_get_order( $order_id ) ) {
				return;
			}

			if ( $order->has_status( 'cancelled' ) ) {
				$action = 'reduce';
			} elseif ( ! $order->has_status( 'cancelled' ) ) {
				$action = 'increase';
			} else {
				return;
			}

			if ( sizeof( $order->get_used_coupons() ) > 0 ) {
				foreach ( $order->get_used_coupons() as $code ) {
					if ( ! $code ) {
						continue;
					}

					$coupon = new \WC_Coupon( $code );

					switch ( $action ) {
						case 'reduce' :
							delete_post_meta( $coupon->get_id(), $_SERVER['REMOTE_ADDR'] . '_used_by_ip' );
						break;
						case 'increase' :
							update_post_meta( $coupon->get_id(), $_SERVER['REMOTE_ADDR'] . '_used_by_ip', '1' );
						break;
					}
				}
			}
		}

		function woocommerce_coupon_is_valid( $return, $coupon ) {
			$used_by_ip = get_post_meta( $coupon->get_id(), $_SERVER['REMOTE_ADDR'] . '_used_by_ip', true );
			if( $used_by_ip == '1' ) {
				throw new \Exception( __( 'You have already used this coupon in a previous order.', 'woocommerce' ) );
			}
			return $return;
		}
	}
}

new Coupon();