<?php
namespace UF;

if( !defined('ABSPATH') ) exit;

if( !class_exists('UF\Messages') ) {
	class Messages {
		function __construct() {
			add_action('wp_ajax_uf_send_customer_message', array( &$this, 'ajax_send_customer_message' ));
			add_action('wp_ajax_nopriv_uf_send_customer_message', array( &$this, 'ajax_send_customer_message' ));
		}

		function ajax_send_customer_message() {
			global $sms;
			if( empty( $_POST['order_id'] ) ) {
				wp_send_json_error('Order ID is empty');
			}
			if( empty( $_POST['email'] ) || !is_email( $_POST['email'] ) ) {
				wp_send_json_error('Wrong email address');
			}
			if( empty( $_POST['message'] ) ) {
				wp_send_json_error('Message is empty');
			}
			if( empty( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], $_POST['order_id'] ) ) {
				wp_send_json_error('Invalid nonce');
			}

			$order_id = (int)$_POST['order_id'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$message = $_POST['message'];

			$order = wc_get_order( $order_id );
			if( !( $order instanceof \WC_Order ) ) {
				wp_send_json_error('Invalid order');
			}

			$order_product = reset($order->get_items());
            $product_id = $order_product->get_product_id();
            $post = get_post( $product_id );
            if( isset( $post->post_author ) ) {
                $user = get_userdata( $post->post_author );
				if( $user instanceof \WP_User ) {
					$subject = 'New Urban Fresh Order';
					$m_template = '<p>A customer has contacted you from Urban Fresh regarding a purchase:<br /><br />
						{{buyer_message}}
						<br /><br />
						Product: {{product}}<br />
						Buyer E-mail: {{buyer_email}}<br />
						Buyer Phone: {{buyer_phone}}<br />
						To respond to the customer, you may reply to this email.<br />
						Thank you for using Urban Fresh<br />
						<a href="https://www.urbanfresh.org/map" target="_blank">www.urbanfresh.org</a>';

					$email_body = str_replace( array(
						'{{buyer_message}}',
						'{{product}}',
						'{{buyer_email}}',
						'{{buyer_phone}}',
					), array(
						$message,
						$post->post_title,
						$email,
						!empty( $phone ) ? $phone : ''
					), $m_template );

                	wp_mail( $user->user_email, $subject, $email_body, array( 'From: ' . $email, 'Content-Type: text/html' ) );

                	$store_info = dokan_get_store_info( $post->post_author );

                	if( !empty( $store_info['phone'] ) ) {
                		$sms->to  = array( $store_info['phone'] );
						$template_vars  = apply_filters( 'uf_sms_template_vars', array(
							'%buyer_message%'     => $message,
							'%product%'           => $post->post_title,
							'%buyer_email%'       => $email,
							'%buyer_phone%'       => !empty( $phone ) ? $phone : '',
						), 'wc_order_customer_message', array( $order_id ) );
						$sms->msg       = str_replace( array_keys( $template_vars ),
							array_values( $template_vars ),
							"A customer has contacted you from Urban Fresh regarding a purchase:
\n%buyer_message%
\nProduct: %product%
Buyer E-mail: %buyer_email%
Buyer Phone: %buyer_phone%" );

						$sms->SendSMS();
	                }

                	wp_send_json_success();
                }
            }
			wp_send_json_error('Unknown error');
		}
	}
}

new Messages();