<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<style type="text/css">
	
	button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {
    background-color: green;
    border-color: green;
    color: #ffffff;
}
button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {
    background-color: green;
    border-color: green;
    color: #ffffff;
}
</style>

	<!--<header>
		<h2 class="text-center">Product Details</h2>
	</header>-->
    
<!--       <div class="product_details_page_images"><img src="//www.urbanfresh.org/wp-content/uploads/2017/08/cingant-crop.jpg" /></div>
	<div class="product_details_page_title">
		<h3 class="producttitle">Fresh Vegie From The Farm, Picking By Your Own</h3>
    </div>
    
    <div class="product_details_page_author">By <a href="#">Arkul Paul</a></div>
    <div class="product_details_page_content">
    	<h5>Description</h5>
    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
        </p><div><a href="#" class="reamore">Read More</a></div>
                
        <p></p>
    </div>
    
    <div class="product_details_additionalinfo">
	<div><h5>Seller info</h5>
    <p>Jl. Pandawa 19 Bali, Indonesia</p>
    <a href="#">Contact seller</a>
    
    
    </div>
    <div>
    	<h5>Shoping info</h5>
        <p>10:00 - 21:00</p>    
    </div>

</div>


<div class="product_details_page_quatity">
	<h5>Quantity</h5>

	<div class="columns">
    	<div align="left"><a href="#" class="minus-sign">-</a></div>
        <div align="center">1</div>
        <div align="right"><a href="#" class="plus-sign">+</a></div>
    
    </div>
    <div align="right">/Kilograme</div>


</div>

<div class="product_details_page_totalprice">
<h5>Total Price</h5>
	<div class="columns">
    	<div class="price_column">
            
            <span class="totalprice">$20</span><span class="pricerate">$10 /Kg</span>
    	</div>
        
        <div class="buynow_button"><a href="#">Buy Now</a></div>
    </div>
</div>

</div> --> <!----->

	<section class="container-fluid product">
		<div class="row">
		
		
		
		

<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary product_details_page">

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
			add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 35 );
			do_action( 'woocommerce_single_product_summary' );
		?>

	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>



	

</div>