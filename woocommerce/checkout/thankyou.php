<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wp_enqueue_script('wp-util');
wp_enqueue_script('uf_jquery_phone_js', get_stylesheet_directory_uri() . '/js/jquery.phone/intlTelInput.min.js', array('jquery') );

wp_enqueue_style('uf_jquery_phone_css', get_stylesheet_directory_uri() . '/assets/css/intlTelInput.css' );
?>

<div class="woocommerce-order woocommerce-order-own-thankyou-page">

	<?php if ( $order ) : ?>

		<?php if ($order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed msg-error-red"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay btn-custom-color-green"><?php _e( 'Pay', 'woocommerce' ) ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay btn-custom-color-green"><?php _e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received msg-success-green"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); ?></p>
            <h2><?php _e( 'Order details', 'woocommerce' ); ?></h2>
            <?php do_action( 'woocommerce_thankyou_own', $order->get_id() ); ?>

            <?php
            $order_items = $order->get_items();
            $order_product = reset($order_items);
            $phone = $email = '';
            if( $order_product ) {
                $product_id = $order_product->get_product_id();
                $post = get_post( $product_id );
                if( isset( $post->post_author ) ) {
                    $vendor = get_userdata( $post->post_author );
                    $store_info = dokan_get_store_info( $post->post_author );
                    if ($store_info['show_email'] === 'yes') {
                        $email = $vendor->get('user_email');
                    }
                    if ( !empty( $store_info['phone'] ) ) {
                        $phone = $store_info['phone'];
                    }

                    if( !empty( $email ) || !empty( $phone ) ) { ?>
                        <h2 class="woocommerce-order-overview__order order"><?php _e( 'Seller details:', 'woocommerce' ); ?></h2>
                        <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
                            <?php if( !empty( $email ) ) { ?>
                                <li class="woocommerce-order-overview__order order">
                                    <?php _e( 'Email:', 'woocommerce' ); ?>
                                    <strong><?php echo $email; ?></strong>
                                </li>
                            <?php } ?>
                            <?php if( !empty( $phone ) ) { ?>
                                <li class="woocommerce-order-overview__order order">
                                    <?php _e( 'Phone:', 'woocommerce' ); ?>
                                    <strong><?php echo $phone; ?></strong>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php }

                }
            }
            ?>
            <div class="thankyou-communicate-form">
                <h2><?php _e('Please communicate with the seeler regarding your purchase', 'woocommerce'); ?></h2>
                <label>
                    <?php _e('Your email address', 'woocommerce'); ?> <span class="required">*</span><br />
                    <input type="text" id="customer_email" name="customer_email" value="" style="width: 100%;" />
                </label><br />
                <label>
                    <?php _e('Your phone number', 'woocommerce'); ?><br />
                    <input id="customer_phone" name="customer_phone" style="width: 100%;" value="<?php echo $phone; ?>" placeholder="<?php _e( 'phone without country code', 'dokan' ); ?>" class="dokan-form-control input-md" type="tel" />
                    <input type="hidden" name="phone" id="customer_phone_hidden"  value="<?php echo $phone; ?>" />
                </label><br />
                <label>
                    <?php _e('Message', 'woocommerce'); ?> <span class="required">*</span><br />
                    <textarea id="customer_message" name="customer_message" rows="3"></textarea><br/>
                    <span class="uf_remain">Available chars: <span>140</span></span>
                </label><br />
				<div class="thankyou-communicate-form-button">
					<input type="button" id="send_message" data-order-id="<?php echo $order->get_id() ?>"
						   data-nonce="<?php echo wp_create_nonce( $order->get_id() ) ?>"
						   value="<?php _e('Message Seller', 'woocommerce'); ?>"
						   style="margin-top: 10px;"
						   class="btn-custom-color-green" />
				</div>
            </div>

		<?php endif; ?>

        <div class="order-hide-payment-method">
            <?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
        </div>

		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

	<?php endif; ?>

</div>

<script type="text/javascript">
(function($) {
    $(function() {
        $('#send_message').on('click', function() {
            var order_id = $(this).data('order-id'),
                nonce = $(this).data('nonce'),
                email = jQuery('#customer_email').val(),
                phone = jQuery('#customer_phone_hidden').val(),
                message = jQuery('#customer_message').val();

            $(this).next('span').remove();

            if( email == '' ) {
                $(this).after('<span class="required">Please enter email</span>');
                return false;
            }
            if( message == '' ) {
                $(this).after('<span class="required">Please enter message</span>');
                return false;
            }

            var $obj = $(this);
            wp.ajax.send('uf_send_customer_message',{
                method: 'POST',
                data: {
                    order_id : order_id,
                    email : email,
                    phone : phone,
                    message : message,
                    nonce : nonce
                },
                success : function( response ) {
                    $obj.after('<span>Message has sent</span>');
                    window.location = '/map';
                },
                error : function( response ) {
                    $obj.after('<span class="required">' + response + '</span>');
                }
            });
        });

        $('#customer_message').on('keypress keyup', function(e) {
            var tval = $(this).val(),
                tlength = tval.length,
                set = 140,
                remain = parseInt(set - tlength);
            $('.uf_remain > span').text(remain);
            if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
                $(this).val( (tval).substring(0, set) );
                return false;
            }
        });

        $("#customer_phone").on("countrychange keyup", function() {
            $('#customer_phone_hidden').val( $(this).intlTelInput("getNumber") );
        }).intlTelInput({
            utilsScript : '<?php echo get_stylesheet_directory_uri() . '/js/jquery.phone/utils.js' ?>'
        });
    });
})(jQuery);
</script>
