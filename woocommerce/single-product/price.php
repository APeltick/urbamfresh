<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
?>
    <h5>Total Price</h5>
	<div class="columns">
    	<div class="price_column">
            <span class="currency"><?php echo get_woocommerce_currency_symbol( $product->currency );?></span>
            <span data-uf-product-price="<?php echo $product->get_price();?>"
                  data-uf-thousand-separator="<?php echo wc_get_price_thousand_separator();?>"
                  data-uf-decimal-separator="<?php echo wc_get_price_decimal_separator();?>"
                  data-uf-decimals="<?php echo wc_get_price_decimals();?>"
                  class="uf-totalprice"><?php //echo $product->get_price_html();
                //echo $product->get_price(); ?></span>
    	</div>
        
        <!-- <div class="buynow_button"><a href="#">Buy Now</a></div> -->
    </div>
</div>