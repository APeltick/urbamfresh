<?php

if ( ! defined( 'ABSPATH' ) ) {

    exit;

}



if ( $query->have_posts() ) : ?>



    <section class="related products">

    <!--<section class="uf-seller-related-products">-->



        <h2 style="text-align: center"><?php esc_html_e( 'More From This Seller', 'woocommerce' ) ?></h2>



        <?php woocommerce_product_loop_start(); ?>

        <?php while ( $query->have_posts() ) : $query->the_post();



        wc_get_template_part( 'content', 'product' );



        endwhile; ?>

        <?php woocommerce_product_loop_end(); ?>



    </section>



<?php endif;



wp_reset_postdata();