<?php
/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( $max_value && $min_value === $max_value ) {
	?>
	<div class="quantity hidden">
		<input type="hidden" class="qty" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $min_value ); ?>" />
	</div>
	<?php
} else {
	?>
	<div class="quantity">
	<div class="heading">
		<h3>Quantity</h3>
	</div>
	<!-- <form id='myform' method='POST' action='#'> -->
	    <div class="qtygroup">
	    <div class='qtyminus' field='<?php echo esc_attr( $input_name ); ?>' /><i class="fa fa-minus" aria-hidden="true"></i></div>
	    <!-- <i class="fa fa-minus-square-o" aria-hidden="true"></i> -->
	    <!-- <input type='text' name='quantity' value='0' class='qty' /> -->
		<input type="number" class="input-text qty text" step="<?php echo esc_attr( $step ); ?>" min="<?php echo esc_attr( $min_value ); ?>" max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $input_value ); ?>" title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ) ?>" size="4" pattern="<?php echo esc_attr( $pattern ); ?>" inputmode="<?php echo esc_attr( $inputmode ); ?>" />
	    
	    <div class='qtyplus' field='<?php echo esc_attr( $input_name ); ?>' /><i class="fa fa-plus" aria-hidden="true"></i></div>

	    <!-- <i class="fa fa-plus-square-o" aria-hidden="true"></i> -->
	    </div><br>

	    <p class="variable" style="text-align: center; margin: 0;"><?php echo mb_substr( $product->get_price_suffix(), 1 ); ?></p>
	<!-- </form> -->
	</div>

<style type="text/css">
	.quantity .input-text,.quantity input[type=email],.quantity input[type=password],.quantity input[type=search],.quantity input[type=text],.quantity input[type=url],.quantity textarea {
	    padding: .6180469716em;
	    background-color: transparent;
	    color: #43454b;
	    outline: 0;
	    border: 0;
	    -webkit-appearance: none;
	    box-sizing: border-box;
	    font-weight: bolder;
	    box-shadow: none;
	}
	button, input[type="button"], input[type="reset"], input[type="submit"], .button, .added_to_cart, .widget a.button, .site-header-cart .widget_shopping_cart a.button {
	    background-color: #fff;
	    border-color: rgba(0, 128, 0, 0.37);
	    color: #000;
	}
	button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, .added_to_cart:hover, .widget a.button:hover, .site-header-cart .widget_shopping_cart a.button:hover {
	    background-color: #fff;
	    border-color: rgba(0, 128, 0, 0.37);
	    color: #000;
	}
	.qtyplus, .qtyminus {
	    border: 1px solid #EFEFEF;
	    color: #4D4D4D;
	    padding: 9px 15px;
	    width: 45px;
	    height: 45px;
	}
	input.input-text.qty.text {
	    font-size: 33px;
	    color: #649D32;
	    padding: 0px;
	    height: 45px;
	    margin: 0px 30px;
	}
	.qtygroup {
	    display: inline-flex;
	}
</style>

	<script type="text/javascript">
		jQuery(document).ready(function($){

		    // number format function
            // Format a number with grouped thousands
            function number_format( number, decimals, dec_point, thousands_sep ) {

                var i, j, kw, kd, km;
                // input sanitation & defaults
                if( isNaN(decimals = Math.abs(decimals)) ){
                    decimals = 2;
                }
                if( dec_point === 'undefined' ){
                    dec_point = ",";
                }
                if( thousands_sep === 'undefined' ){
                    thousands_sep = ".";
                }
                i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

                if( (j = i.length) > 3 ){
                    j = j % 3;
                } else{
                    j = 0;
                }

                km = (j ? i.substr(0, j) + thousands_sep : "");
                kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
                //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
                kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

                return km + kw + kd;
            }

            const $ufTotalprice   = $('.uf-totalprice');
            const $ufProductPrice = $ufTotalprice.data('uf-product-price');
            const ufDecimals      = $ufTotalprice.data('uf-decimals');
            const ufDecSep        = $ufTotalprice.data('uf-decimal-separator');
            const ufThousandsSep  = $ufTotalprice.data('uf-thousand-separator');

            // Product price on page load
            $ufTotalprice.html(number_format( $ufProductPrice, ufDecimals, ufDecSep, ufThousandsSep ));

		    // This button will increment the value
		    jQuery('.qtyplus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        fieldName = jQuery(this).attr('field');
		        // Get its current value
		        var currentVal = parseInt(jQuery('input[name='+fieldName+']').val());
		        // If is not undefined
		        if (!isNaN(currentVal)) {
		            // Increment
		            jQuery('input[name='+fieldName+']').val(currentVal + 1);
		        } else {
		            // Otherwise put a 1 there
		            jQuery('input[name='+fieldName+']').val(1);
		        }
		        //display new total price
                $ufTotalprice.html(number_format( $ufProductPrice * parseInt($('input[name='+fieldName+']').val()), ufDecimals, ufDecSep, ufThousandsSep ));
            });
		    // This button will decrement the value till 1
		    jQuery(".qtyminus").click(function(e) {
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        fieldName = jQuery(this).attr('field');
		        // Get its current value
		        var currentVal = parseInt(jQuery('input[name='+fieldName+']').val());
		        // If it isn't undefined or its greater than 1
		        if (!isNaN(currentVal) && currentVal > 1) {
		            // Decrement one
		            jQuery('input[name='+fieldName+']').val(currentVal - 1);
		        } else {
		            // Otherwise put a 1 there
		            jQuery('input[name='+fieldName+']').val(1);
		        }
		        //display new total price
                $ufTotalprice.html(number_format( $ufProductPrice * parseInt($('input[name='+fieldName+']').val()), ufDecimals, ufDecSep, ufThousandsSep ));
            });
		});

	</script>
	<?php
}
