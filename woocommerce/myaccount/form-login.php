<?php


/**


 * Login Form


 *


 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.


 *


 * HOWEVER, on occasion WooCommerce will need to update template files and you


 * (the theme developer) will need to copy the new files to your theme to


 * maintain compatibility. We try to do this as little as possible, but it does


 * happen. When this occurs the version of the template file will be bumped and


 * the readme will list any important changes.


 *


 * @see     https://docs.woocommerce.com/document/template-structure/


 * @author  WooThemes


 * @package WooCommerce/Templates


 * @version 2.6.0


 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly

}


?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>


<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>


<div class="u-columns col2-set" id="customer_login">

    <div class="u-column1 col-1">

        <?php endif; ?>

<script>
jQuery(document).ready( function(jQuery) {

    if( window.location.href.indexOf( "showlogin" ) > -1) {
        jQuery( "#registerpart" ).css( 'display', 'none' );
        jQuery( "#loginpart" ).css( 'display', 'block' );
        //window.location.reload(false);
    }
    if( window.location.href.indexOf( "showregister" ) > -1) {
        jQuery( "#registerpart" ).css( 'display', 'block' );
        jQuery( "#loginpart" ).css( 'display', 'none' );
        //window.location.reload(false);
    }

    jQuery('body').on('click', '.handheld-navigation a', function() {
        window.location.reload(false);
    });

//window.history.pushState('page2', 'Title', '/');

 });
</script>


<script type='text/javascript' src='https://www.urbanfresh.org/wp-includes/js/zxcvbn-async.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://www.urbanfresh.org/wp-admin/js/password-strength-meter.min.js?ver=4.8.1'></script>
<script type='text/javascript' src='//www.urbanfresh.org/wp-content/plugins/woocommerce/assets/js/frontend/password-strength-meter.min.js?ver=3.1.2'></script>

<div id="loginpart">
        <form class="woocomerce-form woocommerce-form-login login" method="post">

            <?php do_action( 'woocommerce_login_form_start' ); ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="username"><?php _e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
            </p>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" minlength="6" />
            </p>

            <p class="form-row">
                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                <input type="submit" class="woocommerce-Button button login-btn-width" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
                <input name="rememberme" type="hidden" value="forever" />

            </p>

            <p class="woocommerce-LostPassword lost_password">
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
            </p>

            <?php do_action( 'woocommerce_login_form_end' ); ?>
            <div class="login-or-separator">
                <span class="h6 login-or-separator-text">or</span>
                <hr>
            </div>
            <?php do_action( 'woocommerce_login_form' ); ?>

        </form>

        <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

    </div>

</div>

<div id="registerpart">
    <div class="u-column2 col-2">

        <h2><?php _e( 'Register', 'woocommerce' ); ?></h2>

        <form method="post" class="register">
            <?php do_action( 'woocommerce_register_form_start' ); ?>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                    <input type="hidden" name="vendor" value="vendor" />
                </p>

            <?php endif; ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <label for="reg_email"><?php _e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
            </p>

            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" minlength="6" required/>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <label for="confirm_password"><?php _e( 'Confirm Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                    <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="confirm_password" id="confirm_password" minlength="6" required/>
                </p>


            <?php endif; ?>

            <!-- Spam Trap -->


            <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" autocomplete="off" /></div>

            <?php do_action( 'woocommerce_register_form' ); ?>

            <p class="woocomerce-FormRow form-row">
                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                <input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" id="reg_account" />
            </p>

            <?php do_action( 'woocommerce_register_form_end' ); ?>

        </form>

    </div>
</div>

</div>

<script>
jQuery(function($) {
$(".register").validate({
	rules: {
		reg_password: {
		 required: true,
			minlength: 6,

	   },
	   confirm_password: {
		equalTo: "#reg_password",
		 minlength: 6,
	   }
	}
});
jQuery.extend(jQuery.validator.messages, {
    equalTo: "Passwords don't match."
});
});
</script>
<style>
.error {
	color:red;
}
</style>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
