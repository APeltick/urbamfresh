<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
//
// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php post_class(); ?>>
<?php

$product_post = get_post( $product->get_id() );
$store_info = dokan_get_store_info( $product_post->post_author );

// hours-type-1 - double row
// hours-type-2 - single row
// hours-type-3 - zero rows

$additional_class = ' hours-type-2';

if ( isset( $store_info['shop_hours_type'] ) && $store_info['shop_hours_type'] == 'set_hours' ) {
    if ( !empty($store_info['store_from']) && !empty($store_info['store_to']) ) {
        if (!empty( $store_info['store_days'])) {
            $additional_class = ' hours-type-1';
        }
    }
    else {
        $additional_class = ' hours-type-3';
    }
} else {
    $additional_class = ' hours-type-2';
}

?>
	 <div class="product_container<?php echo $additional_class; ?>">

		<div class="product-details product-details-own">

        <?php
        	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );

		?>

            <div class="mapproductextrafield">
                <div><!--ratings--></div>
                <div class="uf-open-time">
                    <?php

                    if ( isset( $store_info['shop_hours_type'] ) && $store_info['shop_hours_type'] == 'set_hours' ) {
                        if ( !empty($store_info['store_from']) && !empty($store_info['store_to']) ) {
                            echo '<i class="fa fa-clock-o"></i>';
                            echo esc_html( $store_info['store_from'] . "-" . $store_info['store_to'] );
                            echo '<br />';
                            echo uf_get_shop_days( isset( $store_info['store_days'] ) ? $store_info['store_days'] : '' );
                        }
                    } else {
                        echo '<i class="fa fa-clock-o"></i> ';
                        _e('Appointment only');
                    }
                ?>
                </div>
            </div>
            <div class="price-details-wrapper">
                <div class="totalprice"><?php echo $product->get_price_html();?></div>
                <div class="product-details-link"><a href="<?php echo get_permalink(); ?>">See Details</a></div>
            </div>
        </div>
		<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );
	?>

    </div>

	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
