(function($) {
    $(document).ready(function() {
        $('.open_contact_form_popup, #menu-item-1889 a').fullScreenPopup();

        if( typeof uf_add_product != 'undefined' ) {
            jQuery('.dokan-add-new-product').trigger('click');
        }

        $('#menu-item-1889').on('click',function() {
            $('#feedback_open_popup').trigger('click');
        });

        $('#contact_form_popup').on('click', '#contact_form_send', function() {
            var user_id = $(this).data('user_id'),
                product_id = $(this).data('product_id'),
                name = $('#contact_form_popup input[name="name"]').val(),
                email = $('#contact_form_popup input[name="email"]').val(),
                message = $('#contact_form_popup textarea[name="message"]').val(),
                error = 0;

            $('.uf_form_error').remove();

            if( name == '' ) {
                $('#contact_form_popup input[name="name"]').after('<span class="uf_form_error">Please enter your name</span>');
                error++;
            }

            if( email == '' || !validateEmail( email ) ) {
                $('#contact_form_popup input[name="email"]').after('<span class="uf_form_error">Please enter your email</span>');
                error++;
            }

            if( error ) return false;

            wp.ajax.send('uf_send_contact_form', {
                method: 'POST',
                data : {
                    user_id : user_id,
                    product_id : product_id,
                    name : name,
                    email : email,
                    msg : message
                },
                beforeSend : function() {
                    jQuery('#wptime-plugin-preloader, #wptime-plugin-preloader-overlay').fadeIn();
                }
            }).always(function() {
                jQuery('#wptime-plugin-preloader, #wptime-plugin-preloader-overlay').fadeOut("slow");
            }).done(function() {
                var form_html = $('#contact_form_popup').html();
                $('#contact_form_popup').html('<span class="uf_success_message">Your message has been delivered to the seller</span>');
                setTimeout(function() {
                    $('.fsp-close').trigger('click');
                    $('#contact_form_popup').html(form_html);
                }, 3000);
            });
        });

        $('#feedback_form_popup').on('click', '#feedback_form_send', function() {
            var name = $('#feedback_form_popup input[name="name"]').val(),
                email = $('#feedback_form_popup input[name="email"]').val(),
                message = $('#feedback_form_popup textarea[name="message"]').val(),
                error = 0;

            $('.uf_form_error').remove();

            if( name == '' ) {
                $('#feedback_form_popup input[name="name"]').after('<span class="uf_form_error">Please enter your name</span>');
                error++;
            }

            if( email == '' || !validateEmail( email ) ) {
                $('#feedback_form_popup input[name="email"]').after('<span class="uf_form_error">Please enter your email</span>');
                error++;
            }

            if( error ) return false;

            wp.ajax.send('uf_send_contact_form', {
                method: 'POST',
                data : {
                    name : name,
                    email : email,
                    msg : message
                },
                beforeSend : function() {
                    jQuery('#wptime-plugin-preloader, #wptime-plugin-preloader-overlay').fadeIn();
                }
            }).always(function() {
                jQuery('#wptime-plugin-preloader, #wptime-plugin-preloader-overlay').fadeOut("slow");
            }).done(function() {
                var form_html = $('#feedback_form_popup').html();
                $('#feedback_form_popup').html('<span class="uf_success_message">Your message has been delivered to the administrator</span>');
                setTimeout(function() {
                    $('.fsp-close').trigger('click');
                    $('#feedback_form_popup').html(form_html);
                }, 3000);
            });
        });
    });


    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,8})?$/;
        return emailReg.test( $email );
    }

})(jQuery);