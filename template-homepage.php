<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

if( $_SERVER['HTTP_USER_AGENT'] !== "Urban Fresh Android") {
	$myfile = file_put_contents('logs.txt', $_SERVER["REQUEST_URI"] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'] . "\n", FILE_APPEND | LOCK_EX);
	header("Location: https://www.urbanfresh.org/app.html");
	exit;
}
?>

