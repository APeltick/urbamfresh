<?php
spl_autoload_register(
    function ($class) {
        $fileName = get_theme_file_path() . '/' . str_replace('\\', '/', $class) . '.php';
        if (file_exists($fileName)) {
            require_once $fileName;
        }
    }
);