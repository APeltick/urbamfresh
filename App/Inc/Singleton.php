<?php

namespace App\Inc;


trait Singleton {
    public static $instance	 = NULL;

    public static function instance() {
        if (null === static::$instance) {
            static::$instance = new static;
        }
        return static::$instance;
    }
}