<?php

namespace App;


use App\Inc\Singleton;
use App\Models\Store;

class ApplicationCore {

    use Singleton;

    protected function __construct() {
        Store::instance();
    }
}