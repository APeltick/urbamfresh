<?php

namespace App\Models;


use App\Inc\Singleton;
use \Datetime;

class Store {

    use Singleton;

    protected function __construct() {
        if ($_SERVER['REMOTE_ADDR'] == '178.165.51.207' || $_SERVER['REMOTE_ADDR'] == '37.57.0.213') {
            if( strpos( $_SERVER['PHP_SELF'], 'location.php' ) === false ) {
                add_action('init', array( self::class, 'cc_plugins_loaded' ) );
            }
        }
        add_action('dokan_store_profile_saved', array( self::class, 'cc_dokan_store_profile_saved' ), 10, 2 );
        add_filter( 'dokan_seller_wizard_steps', array( self::class, 'cc_dokan_seller_wizard_steps' ) );
    }

    public static function cc_plugins_loaded() {
        if ( !( isset( $_COOKIE['uf_flag'] ) && $_COOKIE['uf_flag'] == '1' ) &&
            !isset( $_COOKIE['latitude'] ) && !isset( $_COOKIE['longitude'] ) ) {

            $uf_record = self::get_coordinates_by_ip();
            self::uf_set_cookies($uf_record['lat'], $uf_record['lng'], $uf_record['city']);
            // var_dump - delete next line
            setcookie('location_source', 'from ip', time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
            wp_redirect( (is_ssl() ? 'https://' : 'http://').$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] );
        }

        if( is_user_logged_in() ) {
            $user_id = get_current_user_id();
            $latitude = get_user_meta( $user_id, 'uf_latitude', true );
            $longitude = get_user_meta( $user_id, 'uf_longitude', true );



            if( $latitude != '' && $longitude != '' &&
                isset( $_COOKIE['latitude'] ) && $_COOKIE['latitude'] != '' &&
                isset( $_COOKIE['longitude'] ) && $_COOKIE['longitude'] != '' && need_update_location() ) {
                if( $_COOKIE['latitude'] != $latitude || $_COOKIE['longitude'] != $longitude ) {
                    update_user_meta( $user_id, 'uf_latitude', $_COOKIE['latitude'] );
                    update_user_meta( $user_id, 'uf_longitude', $_COOKIE['longitude'] );
                }
            }

        }
    }

    public static function get_coordinates_by_ip( $ip = '' ) {
        global $ip_coordinates_cache;
        if( !empty( $ip_coordinates_cache ) ) return $ip_coordinates_cache;
        $ip = $ip ? $ip : $_SERVER['REMOTE_ADDR'];
        $response = wp_remote_get( 'http://freegeoip.net/json/' . $ip );
        if( !is_wp_error( $response ) ) {
            $body = wp_remote_retrieve_body( $response );
            $array = json_decode( $body, true );
            if( isset( $array['latitude'] ) && isset( $array['longitude'] ) ) {
                $ip_coordinates_cache[ $ip ] = array(
                    'lat' => $array['latitude'],
                    'lng' => $array['longitude'],
                    'city' => isset( $array['city'] ) ? $array['city'] : ''
                );
                return $ip_coordinates_cache[ $ip ];
            }
        }
        return false;
    }

    public static function uf_set_cookies ($latitude, $longitude, $city) {
        if( !empty( $_COOKIE['latitude'] ) && !empty( $_COOKIE['longitude'] ) &&
            $_COOKIE['latitude'] == $latitude && $_COOKIE['longitude'] == $longitude ) return true;

        /*------PREVENT GETTING GPS LOCATION in not working hours------*/
        if( is_user_logged_in() && !self::check_gps_track() ) {
            var_dump(self::check_gps_track());
            return false;
        }
        /*------PREVENT GETTING GPS LOCATION in not working hours------*/

        setcookie('latitude', $latitude, time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
        setcookie('longitude', $longitude, time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );

        if ( is_user_logged_in() ) {
            if ( get_user_meta( get_current_user_id(), 'uf_latitude', true ) == $latitude && get_user_meta( get_current_user_id(), 'uf_longitude', true ) == $longitude )
                return false;
        }

        $api_key = dokan_get_option( 'gmap_api_key', 'dokan_general', false );
        $response = wp_remote_get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&key=' . $api_key );
        if( is_wp_error( $response ) || empty( $response['body'] ) ) {
            $gm_data = json_decode( $response['body'], true );
        } else {
            $gm_data = array();
        }

        if( isset( $gm_data['status'] ) && $gm_data['status'] == 'OK' && !empty( $gm_data['results'][0]['formatted_address'] ) ) {
            setcookie('address', $gm_data['results'][0]['formatted_address'], time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );
        }

        if ( is_user_logged_in() ) {
            update_user_meta(get_current_user_id(), 'uf_latitude', $latitude);
            update_user_meta(get_current_user_id(), 'uf_longitude', $longitude);
        }
        $seller_settings = get_user_meta(get_current_user_id(), 'dokan_profile_settings', true);
        if( isset( $gm_data['status'] ) && $gm_data['status'] == 'OK' &&
            isset( $gm_data['results'][0]['address_components'] ) &&
            is_array( $gm_data['results'][0]['address_components'] ) ) {

            $data = array(
                'city' => isset( $city ) ? $city : ''
            );

            foreach( $gm_data['results'][0]['address_components'] as $component ) {
                if( isset( $component['types'] ) && in_array( 'country', $component['types'] ) && isset( $component['short_name'] ) ) {
                    $data['country_code'] = $component['short_name'];
                    continue;
                }

                if( isset( $component['types'] ) && in_array( 'postal_code', $component['types'] ) && isset( $component['short_name'] ) ) {
                    $data['zip'] = $component['short_name'];
                    continue;
                }

                if( isset( $component['types'] ) && in_array( 'administrative_area_level_1', $component['types'] ) && isset( $component['short_name'] ) ) {
                    $data['state'] = $component['long_name'];
                    $data['state_code'] = $component['short_name'];
                    continue;
                }

                if( isset( $component['types'] ) && in_array( 'route', $component['types'] ) && isset( $component['short_name'] ) ) {
                    $data['street_1'] = $component['short_name'];
                    continue;
                }

                if( isset( $component['types'] ) && in_array( 'street_number', $component['types'] ) && isset( $component['short_name'] ) ) {
                    $data['street_2'] = $component['short_name'];
                    continue;
                }
            }

            setcookie('uf_location', serialize( $data ), time() + 360 * DAY_IN_SECONDS, '/', COOKIE_DOMAIN );

            $update_address = false;
            if ( is_user_logged_in() && isset( $seller_settings["gps"] ) && $seller_settings["gps"] == '1' ) {
                if( isset( $seller_settings["location_type"] ) && $seller_settings["location_type"] == 'stick' ) {
                    $update_address = true;
                } elseif ( isset( $seller_settings["location_type"] ) && $seller_settings["location_type"] == 'stick_store_hours' && self::check_if_store_open_now() ) {
                    $update_address = true;
                }
            }

            if( $update_address ) {
                $seller_settings["address"]["street_1"] = isset($data['street_1']) ? $data['street_1'] : '';
                $seller_settings["address"]["street_2"] = isset($data['street_2']) ? $data['street_2'] : '';
                $seller_settings["address"]["city"]     = isset($data['city']) ? $data['city'] : '';
                $seller_settings["address"]["zip"]      = isset($data['zip']) ? $data['zip'] : '';
                $seller_settings["address"]["country"]  = isset($data['country_code']) ? $data['country_code'] : '';
                $seller_settings["address"]["state"]    = isset($data['state']) ? $data['state'] : '';

                update_user_meta( get_current_user_id(), 'dokan_profile_settings', $seller_settings );
            }

        } else {
            if( is_user_logged_in() && isset( $seller_settings["address"] ) ) {
                unset($seller_settings["address"]);
                update_user_meta( get_current_user_id(), 'dokan_profile_settings', $seller_settings );
            }
        }
        return('');
    }

    public static function check_gps_track( $user_id = '' ) {
        if( !$user_id ) {
            $user_id = get_current_user_id();
        }
        $seller_settings = get_user_meta($user_id, 'dokan_profile_settings', true);

        if ( !$seller_settings['store_online'] || ( !$seller_settings['gps'] && $seller_settings['location_type'] == 'store_hours' ) ) {
            return false;
        }

        if( $seller_settings['location_type'] == 'always' ) {
            return true;
        } else {
            return self::check_if_store_open_now( $user_id, $seller_settings );
        }
    }

    public static function check_if_store_open_now( $user_id = '', $seller_settings = null ) {
        if( !$user_id ) {
            $user_id = get_current_user_id();
        }
        if ( !$seller_settings ) {
            $seller_settings = get_user_meta($user_id, 'dokan_profile_settings', true);
        }
        $seller_settings = get_user_meta($user_id, 'dokan_profile_settings', true);//getting settings of curr store/user
        $time_zone = $seller_settings['timezone'] ? $seller_settings['timezone'] : 0;//SETTING TIMEZONE
        $time = time() + $time_zone*3600;//this is current time with timezone diff;
        $day = substr(gmdate( 'l' , $time), 0, 2);//here we have day code like: Mo, We, Th to compare it with array

        if ( in_array( $day, $seller_settings['store_days'] ) ) {//checking if today is a working day

            $time_from = str_replace(array( '%am%', '%pm%' ),array( ' AM', ' PM' ), $seller_settings['store_from']);
            $time_to = str_replace(array( '%am%', '%pm%' ),array( ' AM', ' PM' ), $seller_settings['store_to']);

            $time_from = gmdate("H:i", strtotime( $time_from ));//HERE WE HAVE normal 24h time like: 12:30
            $time_to = gmdate("H:i", strtotime( $time_to ));

            $current_time = DateTime::createFromFormat('H:i', gmdate("H:i", $time));
            $time_from = DateTime::createFromFormat('H:i', $time_from);
            $time_to = DateTime::createFromFormat('H:i', $time_to);

            if ( $current_time > $time_from && $current_time < $time_to ){//this if has now working store only
                return true;
            } else {
                return false; //skip this shop, it's not working this time
            }
        } else {
            return false;//skip this shop, it's not working today
        }
    }

    public static function cc_dokan_store_profile_saved( $store_id, $profile ) {
//    var_dump($profile);
        // var_dump($store_id);
        if( wp_verify_nonce( $_POST['_wpnonce'], 'dokan_store_settings_nonce' ) ) {
            if( isset( $_POST['dokan_store_from'] ) && isset( $_POST['dokan_store_to'] ) ) {
                $profile['store_from'] = $_POST['dokan_store_from'];
                $profile['store_to'] = $_POST['dokan_store_to'];
            } else {
                $profile['store_from'] = '';
                $profile['store_to'] = '';
            }

            if( isset( $_POST['dokan_store_days'] ) ) {
                $profile['store_days'] = $_POST['dokan_store_days'];
            }

            if( isset( $_POST['dokan_store_online'] ) ) {
                $profile['store_online'] = $_POST['dokan_store_online'];
            } else {
                $profile['store_online'] = '0';
            }

            $profile['shop_hours_type'] = !empty( $_POST['shop_hours_type'] ) ? $_POST['shop_hours_type'] : 'appointment_only';
            $profile['location_type'] = isset( $_POST['location_type'] ) ? $_POST['location_type'] : 'store_hours';
            $profile['gps'] = isset( $_POST['gps'] ) ? $_POST['gps'] : '0';
            $profile['apply_coupons'] = isset( $_POST['apply_coupons'] ) ? 'yes' : 'no';

            $profile['dokan_coupons_street'] = isset( $_POST['dokan_coupons_street'] ) ? $_POST['dokan_coupons_street'] : '';// Street address (Required)
            $profile['dokan_coupons_suite'] = isset( $_POST['dokan_coupons_suite'] ) ? $_POST['dokan_coupons_suite'] : '';// Apartment/suite # (Optional)
            $profile['dokan_coupons_city'] = isset( $_POST['dokan_coupons_city'] ) ? $_POST['dokan_coupons_city'] : '';// City (Required)
            $profile['dokan_coupons_zip'] = isset( $_POST['dokan_coupons_zip'] ) ? $_POST['dokan_coupons_zip'] : '';// Zip code (Required)
            $profile['dokan_coupons_state'] = isset( $_POST['dokan_coupons_state'] ) ? $_POST['dokan_coupons_state'] : '';// State (Required)
            $profile['timezone'] = isset( $_POST['timezone'] ) ? $_POST['timezone'] : '';// timezone of user/store
            $profile['phone'] = isset( $_POST['phone'] ) ? $_POST['phone'] : '';

            update_user_meta( $store_id, 'dokan_profile_settings', $profile );

        } else if( wp_verify_nonce( $_POST['_wpnonce'], 'dokan_payment_settings_nonce' ) ) {
            if ( isset( $_POST['settings'] ) && is_array( $_POST['settings'] ) ) {
                foreach( $_POST['settings'] as $gateway => $settings ) {
                    if( !empty( $profile['payment'][ $gateway ] ) ) {
                        $profile['payment'][$gateway]['enable'] = !empty( $settings['enable'] );
                    }
                }

                update_user_meta( $store_id, 'dokan_profile_settings', $profile );
            }
        }
    }

    public static function cc_dokan_seller_wizard_steps( $steps ) {
        $GLOBALS['dokan_obj'] = $steps['store']['view'][0];
        $steps['store']['view'] = array( self::class, 'cc_dokan_setup_store' );
        $steps['next_steps']['view'] = array( self::class, 'cc_redirect_to_dashboard' );
        $steps['store']['handler'] = array( self::class, 'cc_dokan_setup_store_save' );
        unset( $steps['introduction'] );
        unset( $steps['payment'] );
        return $steps;
    }

    public static function cc_redirect_to_dashboard() {
        ?>
        <style>
            .wc-setup.wp-core-ui {
                display: none;
            }
        </style>
        <script type="text/javascript">
            window.location.href = '//www.urbanfresh.org/myshop/products/';
        </script>
        <?php
        exit;
    }

    public static function cc_dokan_setup_store() {
        global $dokan_obj;
        $store_id   = get_current_user_id();
        $store_info = dokan_get_store_info( $store_id );
        $address_components = !empty( $_COOKIE['uf_location'] ) ? maybe_unserialize( stripslashes( $_COOKIE['uf_location'] ) ) : array();
        $location = array_merge( $address_components, array(
            'latitude' => $_COOKIE['latitude'],
            'longitude' => $_COOKIE['longitude'],
            'address' => $_COOKIE['address']
        ) );

        dokan_get_template_part( 'settings/seller-setup-wizard', '', array(
            'store_id' => $store_id,
            'store_info' => $store_info,
            'location' => $location,
            'dokan_obj' => $dokan_obj
        ) );

        $seller_settings = get_user_meta($store_id, 'dokan_profile_settings', true);
        $seller_settings['store_online'] = '1';
        update_user_meta($store_id,'dokan_profile_settings', $seller_settings);
    }

    public static function cc_dokan_setup_store_save() {
        global $dokan_obj;
        $dokan_obj->store_info['gps'] = !empty( $_POST['gps'] ) ? $_POST['gps'] : '';
        $dokan_obj->store_info['location_type'] = !empty( $_POST['location_type'] ) ? $_POST['location_type'] : 'store_hours';
        $dokan_obj->store_info['show_phone'] = !empty( $_POST['show_phone'] ) ? 'yes' : 'no';
        $dokan_obj->store_info['store_from'] = !empty( $_POST['dokan_store_from'] ) ? $_POST['dokan_store_from'] : '';
        $dokan_obj->store_info['store_to'] = !empty( $_POST['dokan_store_to'] ) ? $_POST['dokan_store_to'] : '';
        $dokan_obj->store_info['store_days'] = !empty( $_POST['dokan_store_days'] ) ? $_POST['dokan_store_days'] : '';
        $dokan_obj->store_info['shop_hours_type'] = !empty( $_POST['shop_hours_type'] ) ? $_POST['shop_hours_type'] : '';
        $dokan_obj->store_info['currency'] = !empty( $_POST['uf_currency'] ) ? $_POST['uf_currency'] : 'USD';
        $dokan_obj->store_info['phone'] = !empty( $_POST['phone'] ) ? $_POST['phone'] : '';


        update_user_meta( get_current_user_id(), 'uf_latitude', $_POST['latitude'] );
        update_user_meta( get_current_user_id(), 'uf_longitude', $_POST['longitude'] );
        $dokan_obj->dokan_setup_store_save();
    }
}