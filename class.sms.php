<?php
namespace UF;

if( !defined('ABSPATH') ) exit;

if( !class_exists('UF\SMS') ) {
	class SMS {
		function __construct() {
			add_filter('wp_sms_to', array( &$this, 'wp_sms_to' ), 999);
			add_filter('uf_sms_template_vars', array( &$this, 'sms_template_vars' ), 999, 3);
		}

		function wp_sms_to( $to ) {
			if( isset( $to[0] ) && $to[0] == '{{admin_number}}' ) {
				$cart = WC()->cart->get_cart();
				$cart_item = reset($cart);
	            if( !empty( $cart_item['product_id'] ) ) {
		            $product_id = $cart_item['product_id'];
		            $post       = get_post( $product_id );
		            if ( isset( $post->post_author ) ) {
			            $store_info = dokan_get_store_info( $post->post_author );
			            $to = !empty( $store_info['phone'] ) ? array( $store_info['phone'] ) : $to;
		            }
	            }
			}
			return $to;
		}

		function sms_template_vars( $vars, $key, $data ) {
			$order_id = $data[0];
			$order    = new \WC_Order( $order_id );
			$vars['%price%']   = html_entity_decode( strip_tags( wc_price( $order->get_total() ) ) );

			$vars['%product_name%'] = '';
			if( !WC()->cart->is_empty() ) {
				$items = array_values( WC()->cart->get_cart() );
				$vars['%product_name%'] = $items[0]['data']->get_title();
			}
			return $vars;
		}
	}
}

new SMS();