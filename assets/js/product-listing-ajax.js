

    jQuery(document).ready( function($) {

        // change post status on click
        $('body').on('click', '.post-status', function(){

            var ufPostId = $(this).data('uf-post-id');

            $.ajax({

                method: "POST",

                url: product_listing.ajax_url,

                dataType: "json",

                data: {

                    action: 'change_post_status',

                    ufPostId: ufPostId

                },

                success: function () {

                    location.reload();

                },

                error: function () {

                    console.log("ajax-error: can't change post status");

                }

            });

        });

        // ajax price
        var typingPriceTimer;
        var doneTypingPriceInterval = 1700;
        var ufProductId;
        var ufNewProductPrice;

        $('body').on('input', '.uf-price-input input', function () {

            // mask price fields
            var uf_price_regexp = new RegExp('^\\d{0,9}[\\.,]{0,1}\\d{0,2}$');
            var val = $(this).val(), match = val.match(uf_price_regexp);
            if( val == '' ) {
                //$(this).val( val );
            }
            if ( val != '' && (match == null || typeof match[0] == 'undefined' || match[0] !== val) ) {
                $(this).val( $(this).data('previous_value') );
                return true;
            }

            ufProductId = $(this).data('uf-product-id');
            clearTimeout(typingPriceTimer);
            typingPriceTimer = setTimeout(donePriceTyping, doneTypingPriceInterval);
            ufNewProductPrice = $(this).val();

        }).on('keydown', '.uf-price-input input', function () {
            var val =  $(this).val();
            $(this).data('previous_value', val);
        });

        function donePriceTyping () {

            $.ajax({

                method: "POST",

                url: product_listing.ajax_url,

                dataType: "json",

                data: {

                    action: 'change_product_price',

                    ufProductId: ufProductId,

                    ufNewProductPrice: ufNewProductPrice

                },

                success: function (data) {

                    var resultSpan = "#uf_price_change_result_" + data;

                    $(resultSpan).fadeIn(700);

                    setTimeout(function () {

                        $(resultSpan).fadeOut(700);

                    }, 5000);

                },

                error: function () {

                    console.log("ajax-error: can't change product price");

                }

            });
        }

    });