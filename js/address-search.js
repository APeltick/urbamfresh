function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var placeSearch, autocomplete;

function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('address[street_1]')),
        {types: ['address']});
}

function restrictGeolocate() {

    var geolocation = {
        lat: parseFloat(getCookie('latitude')),
        lng: parseFloat(getCookie('longitude'))
    };
    var circle = new google.maps.Circle({
        center: geolocation,
        radius: 1
    });
    autocomplete.setBounds(circle.getBounds());

}

function refreshGeolocation() {
    var geocoder =  new google.maps.Geocoder();
    geocoder.geocode( { 'address': jQuery('#address[city]').val()}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng());
        } else {
            console.log("Something got wrong " + status);
        }
    });
}