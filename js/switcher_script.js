jQuery(document).ready( function ($) {
 $('#switcher').draggable({
        axis: "x",
        containment: 'parent',
        drag : function( ui, position, offset ) {
            var position = ( position.position.left );
            $('#substrate-switcher').css('left', -position);
        },
        start : function() {
            $("#switcher").draggable({
                containment: "parent"
            });
        },
        stop : function( ui, position, offset ) {
            var map_link = $('.link-map').attr('data-link');
            var list_link = $('.link-list').attr('data-link');
            //console.log(position.position.left);
            if (position.position.left > 50) {
                $('#switcher').animate({ left: '100px' }, 300);
                $('#substrate-switcher').animate({ left: '-100px' }, 300);
                if( window.location.href != map_link ) {
                    window.location.href = map_link;
                }
            } else {
                $('#switcher').animate({ left: '1' }, 300);
                $('#substrate-switcher').animate({ left: '1' }, 300);
                if( window.location.href != list_link ) {
                    window.location.href = list_link;
                }
            }
        }
    });

});
