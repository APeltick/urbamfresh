(function($) {
    jQuery(document).ready(function () {
        var userLat = parseFloat(getCookie('latitude'));
        var userLng = parseFloat(getCookie('longitude'));
        var $dokan_store_from = jQuery('#dokan_store_from');
        var $dokan_store_to = jQuery('#dokan_store_to');

        if (!$dokan_store_from.val()) {
            $dokan_store_to.val('').prop('disabled', true);
        }

        $("#setting_phone").intlTelInput({
            utilsScript : uf_seller_wizard_var.tel_util_script
        });
        $("#setting_phone").on("countrychange keyup", function() {
            $('#setting_phone_hidden').val( $(this).intlTelInput("getNumber") );
        });

        $dokan_store_from.on('change', function () {
            if (!jQuery(this).val()) {
                $dokan_store_to.val('').prop('disabled', true);
            } else {
                $dokan_store_to.prop('disabled', false);
            }
        });
        $( 'body' ).on('change', '.shop_hours_type', function () {
            if ( $(this).val() == 'set_hours' ) {
                if( $('#uf-store-hours-wrap:visible').length == 0 ) {
                    $('#uf-store-hours-wrap').removeClass('hidden');
                    $( '#dokan_store_from' ).val('');
                    $( '#dokan_store_to' ).val('');
                }
            } else {
                $('#uf-store-hours-wrap').addClass('hidden');
            }
        });

        jQuery('#dokan_store_from, #dokan_store_to').timepicker({
            disableTouchKeyboard : true
        }).on('showTimepicker', function () {
            jQuery('.ui-timepicker-wrapper').width(jQuery(this).outerWidth() + 'px');
        }).on('changeTime', function () {
            var time = jQuery(this).val();
            if (jQuery(this).prop('id') == 'dokan_store_from') {
                $dokan_store_to.timepicker('option', 'minTime', time);
            } else {
                $dokan_store_from.timepicker('option', 'maxTime', time);
            }
        });
    });
})(jQuery);

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

var autocomplete;

function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete( document.getElementById('inputAuto'), {
        types: ['address']
    });
    autocomplete.addListener('place_changed', function() {
        var value,
            componentForm = {
                inputCity: 'locality',
                inputState: 'administrative_area_level_1',
                inputZip: 'postal_code',
                inputCountry: 'country',
                inputStreet_2 : 'street_number',
                inputStreet_1 : 'route',
            };
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            value = search_place( componentForm[component], place.address_components );
            jQuery('#' + component).prop('disabled', false).val( value );
        }

        refreshGeolocation();
    });
}

function search_place(type, placeObject) {
    for (var i = 0; i < placeObject.length; i++) {
        if (placeObject[i].types[0] === type) {
            return placeObject[i].short_name;
        } else if (i === placeObject.length - 1) {
            return "";
        }
    }
}

function refreshGeolocation() {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({
        'address': jQuery('#inputAuto').val()
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log("location : " + results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
            jQuery('#latitude').val( results[0].geometry.location.lat() );
            jQuery('#longitude').val( results[0].geometry.location.lng() );
        }
    });
}