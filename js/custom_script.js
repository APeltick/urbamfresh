(function ($) {


	if ($('.listmapbutton a:nth-child(1)').hasClass("active")) {
		$('.listmapbutton a:nth-child(1)').click(function(){
                      return false;
                });
	} else if ($('.listmapbutton a:nth-child(2)').hasClass("active")) {
		$('.listmapbutton a:nth-child(2)').click(function(){
                      return false;
                });
	}

})(jQuery);

jQuery(document).ready( function ($) {

	$('#site-navigation .handheld-navigation .menu-item a').click( function () {
        $('#site-navigation .menu-toggle').trigger('click');
    } );

	$('#content').click(function () {
		if ( $('#site-navigation').hasClass('toggled') ) {
            $('#site-navigation .menu-toggle').trigger('click');
		}
    });


	$('.product_price').each(function() {
		var price = $(this).val();
		if (price > 999) {
			$(this).css('max-width', 45);
		}
		else if (price > 99) {
			$(this).css('max-width', 40);
		}
		else if (price > 9) {
			$(this).css('max-width', 30);
		}
	});

     var titleHeight = $('.entry-summary h3.producttitle').height();
     var topAvatar = titleHeight-15;
     $('.profile-img img').css({'top': topAvatar});

     $('.product_container').each(function() {
         var productImg = $('.product_container img').height();
         var productDescription = $('.product_container .product-details').height();
         var productBlockHeight = productImg + productDescription + 20;
         $('.single-product .related.products .product_container').css('height', productBlockHeight);

     });
});
jQuery(document).ready(function( $ ) {
    var isset = false;
   $(".product-details.product-details-own  .woocommerce-loop-product__title").dotdotdot({
      callback: function( isTruncated ) {},
      ellipsis: "\u2026 ",
   });
});
