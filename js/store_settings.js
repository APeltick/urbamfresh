(function($) {
    // Store From-To "null" fixer
    $( document ).ready( function () {
        var dokan_address_wrapper = $( '.dokan-address-fields' );
        var $dokan_store_from = $( '#dokan_store_from' );
        var $dokan_store_to = $( '#dokan_store_to' );
        var $dokan_store_online = $( '#dokan_store_online' );
        var $store_form = $( '#store-form' );

        $( '#dokan_store_from, #dokan_store_to' ).timepicker({
            disableTouchKeyboard : true
        }).on('showTimepicker', function() {
            $( '.ui-timepicker-wrapper' ).width( $(this).outerWidth() + 'px' );
        }).on('changeTime', function() {
            var time = $(this).val();
            if( $(this).prop('id') == 'dokan_store_from' ) {
                $dokan_store_to.timepicker( 'option', 'minTime', time );
            } else {
                $dokan_store_from.timepicker( 'option', 'maxTime', time );
            }
        });

        $('#setting_phone').keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .

            if( $.inArray( e.keyCode, [46, 8, 9, 27, 13, 91, 107, 109, 110, 187, 189, 190] ) !== -1 ||
                // Allow: Ctrl+A
                ( e.keyCode == 65 && e.ctrlKey === true ) ||
                // Allow: home, end, left, right
                ( e.keyCode >= 35 && e.keyCode <= 39 ) ) {

                // let it happen, don't do anything
                return;
            }

            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('body').on('click', '.uf-featured-thumbnail-img', function (e) {
            // Change active image on thumbnail click
            var $img_url = $(e.target).css('background-image');
            $img_url = $img_url.replace('url(','').replace(')','').replace(/\"/gi, "");
            $('#cropbox').attr('src', $img_url);
            $(e.target).siblings().removeClass('uf-selected-image');
            $(e.target).addClass('uf-selected-image');
        });

        // IMAGE UPLOADING
        $('#uf-product-pict, #uf-add-new-image-small').each(function() {
            var id = $(this).prop('id');
            var $obj = $(this);
            var plup_settings = {
                runtimes : 'html5,flash,silverlight,html4',
                browse_button: id,
                container: $obj.parent().get(0),
                url: uf_store_settings_var.uploader.async_upload,
                resize: {
                    width: 650,
                    height: 650,
                    preserve_headers: false
                },
                flash_swf_url : uf_store_settings_var.uploader.flash,
                silverlight_xap_url : uf_store_settings_var.uploader.xap,
                filters: {
                    mime_types: [{
                        title: 'Allowed Files',
                        extensions: '*'
                    }]
                },
                max_files: 0,
                file_data_name : "async-upload",
                max_file_size : uf_store_settings_var.uploader.max_upload_size,
                multipart_params : {
                    action : 'upload-attachment',
                    _wpnonce : uf_store_settings_var.uploader.nonce
                },
                init : {
                    FilesAdded: function(up, files) {
                        up.start();

                        $('.uf-featured-thumbnail-img').removeClass('uf-selected-image');

                        var $instruction = $('#uf-intruction-inside');
                        var $wrap = $instruction.siblings('.image-wrap');

                        //$instruction.find('input.dokan-feat-image-id').val('0');
                        $wrap.addClass('dokan-hide');
                        $instruction.removeClass('dokan-hide');

                        $('#uf-remove-image').fadeOut();
                    },
                    UploadProgress: function (up, file) {
                        $('.uf-loading-spin').removeClass('fa-cloud-upload').addClass('fa-refresh fa-3x fa-spin fa-fw');
                    },
                    FileUploaded: function (up, files, response) {
                        response = JSON.parse( response.response );
                        // set the image hidden id
                        $('input.dokan-feat-image-id[name="single_image_id"]').val(response.data.id);
                        $('#profile_image').attr('value',response.data.id);
                        // set the image
                        var $instruction = $('#uf-intruction-inside');
                        var $wrap = $instruction.siblings('.image-wrap');
                        var img = new Image();
                        img.onload = function() {
                            $wrap.find('img').attr('src', response.data.url);
                            $instruction.addClass('dokan-hide');
                            $wrap.removeClass('dokan-hide');
                            $('<div data-image-id="' + response.data.id + '" class="uf-featured-thumbnail uf-featured-thumbnail-img uf-selected-image"></div>').insertBefore("#uf-clearfix").css("background", "url('" + response.data.url + "')");
                            $('#uf-remove-image').fadeIn().css("display","inline-block");
                            $('#uf-featured-thumbnails-wrapper').fadeIn();
                            $('.uf-loading-spin').removeClass('fa-refresh fa-3x fa-spin fa-fw').addClass('fa-cloud-upload');
                        };
                        img.src = response.data.url;
                    }
                }
            };

            uploader = new plupload.Uploader( plup_settings );
            uploader.init();
        });


        if ( !$('#dokan_store_from').val() ) {
            $( '#dokan_store_to' ).val('').prop('disabled', true);
        }

        $( '#dokan_store_from' ).on('change', function () {
            if ( !$(this).val() ) {
                $( '#dokan_store_to' ).val('').prop('disabled', true);
            } else {
                $( '#dokan_store_to' ).prop('disabled', false);
            }
        });

        // Appointment only

        $( 'body' ).on('change', '.shop_hours_type', function () {
            if ( $(this).val() == 'set_hours' ) {
                if( $('#uf-store-hours-wrap:visible').length == 0 ) {
                    $('#uf-store-hours-wrap').removeClass('hidden');
                    $( '#dokan_store_from' ).val('');
                    $( '#dokan_store_to' ).val('');
                }
            } else {
                $('#uf-store-hours-wrap').addClass('hidden');
            }
        });

        if( $('.shop_hours_type:checked').val() != 'set_hours' ) {
            $('#uf-store-hours-wrap').addClass('hidden');
        }

        //****show - hide for coupons settings****
        $('#show_hide_checkbox').on( 'click init_adress_box', function() {
            if ($('#apply_coupons').is(':checked')) {
                $("div.show_hide").show();
            } else {
                $("div.show_hide").hide();
            }
        }).trigger('init_adress_box');
        //****show - hide for coupons settings****

        $("#setting_phone").intlTelInput({
            utilsScript : uf_store_settings_var.tel_util_script
        });
        $("#setting_phone").on("countrychange keyup", function() {
            $('#setting_phone_hidden').val( $(this).intlTelInput("getNumber") );
        });
    });
})(jQuery);

var autocomplete;

function initAutocomplete() {
    autocomplete = new google.maps.places.Autocomplete( document.getElementById('inputAuto'), {
        types: ['address']
    });
    autocomplete.addListener('place_changed', function() {
        var value,
            componentForm = {
                inputCity: 'locality',
                inputState: 'administrative_area_level_1',
                inputZip: 'postal_code',
                inputCountry: 'country',
                inputStreet_2 : 'street_number',
                inputStreet_1 : 'route'
            };
        var place = autocomplete.getPlace();
        console.log("UTC timez_zone: " + place['utc_offset']/60);//this is timezone, formats: -2, 0, 4 etc.
        jQuery('input#timezone').val(place['utc_offset']/60);//set timezone value to the hidden input
        for (var component in componentForm) {
            value = search_place( componentForm[component], place.address_components );
            jQuery('#' + component).prop('disabled', false).val( value );
        }

        refreshGeolocation();
    });
}

function search_place(type, placeObject) {
    for (var i = 0; i < placeObject.length; i++) {
        if (placeObject[i].types[0] === type) {
            return placeObject[i].short_name;
        } else if (i === placeObject.length - 1) {
            return "";
        }
    }
}

function refreshGeolocation() {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({
        'address': jQuery('#inputAuto').val()
    }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            console.log("location : " + results[0].geometry.location.lat() + " " + results[0].geometry.location.lng());
            jQuery('#latitude').val( results[0].geometry.location.lat() );
            jQuery('#longitude').val( results[0].geometry.location.lng() );
        }
    });
}

function delete_info(){
    console.log('safas');
    $("#cropbox").removeAttr('src');
    $("#profile_image").removeAttr('value');
}