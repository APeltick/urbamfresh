<?php

/**

 * The header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="content">

 *

 * @package storefront

 */



?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/custom.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/new.css" type="text/css" media="screen">
</head>

<body <?php body_class(); ?>>
<script>document.body.oncontextmenu = function() {return false;}</script>
<div id="feedback_form_popup" class="uf_form_popup">
    <label>
        Your Name (required)<br />
        <input type="text" name="name" value="" />
    </label>

    <label>
        Your Email (required)<br />
        <input type="email" name="email" value="" />
    </label>
    <label>
        Your Message<br />
        <textarea rows="5" cols="50" name="message"></textarea>
    </label>

    <button class="primary_button" id="feedback_form_send">Send</button>
</div>

<script>
jQuery(function($) {
	$('input[name="dokan_update_payment_settings"]').click(function() {
		$(".site").animate({ scrollTop: 0 }, "slow");
	});

	$('.site_branding .custom-logo-link').click(function(e) {
	    e.preventDefaults();
	    return false;
    });
});
</script>

<!-- Preloader -->
<div id="wptime-plugin-preloader-overlay"></div>
<div id="wptime-plugin-preloader"></div>



<div id="page" class="hfeed site">

	<?php

	do_action( 'storefront_before_header' ); ?>



	<header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">

		<div class="col-full">



			<?php

			/**

			 * Functions hooked into storefront_header action

			 *

			 * @hooked storefront_skip_links                       - 0

			 * @hooked storefront_social_icons                     - 10

			 * @hooked storefront_site_branding                    - 20

			 * @hooked storefront_secondary_navigation             - 30

			 * @hooked storefront_product_search                   - 40

			 * @hooked storefront_primary_navigation_wrapper       - 42

			 * @hooked storefront_primary_navigation               - 50

			 * @hooked storefront_header_cart                      - 60

			 * @hooked storefront_primary_navigation_wrapper_close - 68

			 */

			do_action( 'storefront_header' ); ?>

<?php         
if ( is_user_logged_in() ) {
	$current_user = wp_get_current_user();
	$store_info = dokan_get_store_info( $current_user->ID );
	$store_name = $store_info["store_name"];
	if ( strlen($store_name) > 7 ) {
		$store_short_name = substr($store_name, 0, 6) . '...';
		$html .=  '<a class="uf-store-shortname" href="//www.urbanfresh.org/myshop/products/" style="right: 25px;top: 25px;z-index: 99;	color: #649d32!important;"><i class="fa fa-home"></i> ' . $store_short_name . '</a>';
	} else {
		$html .=  '<a class="uf-store-shortname" href="//www.urbanfresh.org/myshop/products/" style="right: 25px;top: 25px;z-index: 99;	color: #649d32!important;"><i class="fa fa-home"></i> ' . $store_name . '</a>';
	}
	echo $html;
}
?>

		</div>

	</header><!-- #masthead -->



	<?php

	/**

	 * Functions hooked in to storefront_before_content

	 *

	 * @hooked storefront_header_widget_region - 10

	 */

	do_action( 'storefront_before_content' ); ?>



	<div id="content" class="site-content" tabindex="-1">

		<div class="col-full">



		<?php

		/**

		 * Functions hooked in to storefront_content_top

		 *

		 * @hooked woocommerce_breadcrumb - 10

		 */

		do_action( 'storefront_content_top' );

